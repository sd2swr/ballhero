package com.example.mycitrusgame;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.BaseGameActivity;

public class OptionsScreen {
	static Scene scene;
	
	public static void buildScreen(Scene mScene) {
		scene = mScene;
		MainActivity.isInLevelSelector =  true;
		Entity optionsLayer = new Entity();
		Text vibrationText = new Text(
				60,
				200,
				MainActivity.mFont,
				"vibraton:",1000,MainActivity.engine.getVertexBufferObjectManager());
		
		Text soundText = new Text(
				60,
				300,
				MainActivity.mFont,
				"sound:",1000,MainActivity.engine.getVertexBufferObjectManager());
		TiledSprite soundButton = new TiledSprite(200,300,
				MainActivity.mButtonTextureRegion, ((BaseGameActivity) MainActivity.context)
				.getVertexBufferObjectManager()) {

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					if(MainActivity.sound)
					{
						this.setCurrentTileIndex(1);
						MainActivity.sound = false;
					}
					else
					{
						this.setCurrentTileIndex(0);
						MainActivity.sound = true;
					}
					return true;
				}
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		TiledSprite vibrationButton = new TiledSprite(200, 200,
				MainActivity.mButtonTextureRegion, ((BaseGameActivity) MainActivity.context)
				.getVertexBufferObjectManager()) {

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {
				if (pSceneTouchEvent.isActionDown()) {
					if(MainActivity.vibrations)
					{
						this.setCurrentTileIndex(1);
						MainActivity.vibrations = false;
					}
					else
					{
						this.setCurrentTileIndex(0);
						MainActivity.vibrations = true;
					}
					return true;
				}
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		mScene.registerTouchArea(soundButton);
		mScene.registerTouchArea(vibrationButton);
		mScene.attachChild(soundButton);
		mScene.attachChild(vibrationButton);
		optionsLayer.attachChild(soundText);
		optionsLayer.attachChild(vibrationText);
		scene.attachChild(optionsLayer);
	}
}
