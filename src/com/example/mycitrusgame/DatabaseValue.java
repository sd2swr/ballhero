package com.example.mycitrusgame;

public class DatabaseValue {
	public DatabaseValue(String lastLevelPassed,String optionsSound,String optionsVibration)
	{
		this.lastLevelPassed = lastLevelPassed;
		this.optionsSound = optionsSound;
		this.optionsVibration = optionsVibration;
	}
	public String lastLevelPassed;
	public String optionsSound;
	public String optionsVibration;
}
