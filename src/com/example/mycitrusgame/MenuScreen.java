package com.example.mycitrusgame;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.ui.activity.BaseGameActivity;

public class MenuScreen {

	static Scene scene;
	static ButtonSprite beginGameSprite;
	static ButtonSprite optionsSprite;
	static ButtonSprite creditsSprite;
	static ButtonSprite resultsSprite;
	
	public static void buildMenuScreen(Scene mScene) {
		scene = mScene;

		final Entity menuScreen = new Entity();
		mScene.registerUpdateHandler(MainActivity.mPhysicsWorld);
		final float red = 0;
		final float green = 0;
		final float blue = 0;
		final Sprite spriteBackground = new Sprite(0, 0,
				MainActivity.menuBackground,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		SpriteBackground background = new SpriteBackground(red, green, blue,
				spriteBackground);
		mScene.attachChild(menuScreen);

		beginGameSprite = new ButtonSprite(MainActivity.cameraWidth - 600, 225, MainActivity.buttonTextureRegion,
				MainActivity.engine.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {
					Scene pScene = new Scene();
					MainActivity.levelSelector = new LevelSelector(2, MainActivity.cameraWidth, MainActivity.cameraHeight,
							pScene, MainActivity.engine);
					MainActivity.levelSelector.createTiles(MainActivity.levelButtonTR, MainActivity.mFont);
					MainActivity.levelSelector.setVisible(true);
					MainActivity.levelSelector.show();
					MainActivity.engine.setScene(pScene);
				}
				;

				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		
		optionsSprite  = new ButtonSprite(MainActivity.cameraWidth  - 1000, 225, MainActivity.buttonTextureRegion,
				MainActivity.engine.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {
					Scene pScene = new Scene();
					OptionsScreen.buildScreen(pScene);
					MainActivity.engine.setScene(pScene);
				}
				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		creditsSprite  = new ButtonSprite(MainActivity.cameraWidth - 600, 400, MainActivity.buttonTextureRegion,
				MainActivity.engine.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {

					Scene pScene = new Scene();
					CreditsScreen.buildScreen(pScene);
					MainActivity.engine.setScene(pScene);
				}

				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		resultsSprite  = new ButtonSprite(MainActivity.cameraWidth - 1000, 400, MainActivity.buttonTextureRegion,
				MainActivity.engine.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {

				}

				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		mScene.registerTouchArea(beginGameSprite);
		mScene.registerTouchArea(optionsSprite);
		mScene.registerTouchArea(creditsSprite);
		mScene.registerTouchArea(resultsSprite);
		menuScreen.attachChild(beginGameSprite);
		menuScreen.attachChild(optionsSprite);
		menuScreen.attachChild(creditsSprite);
		menuScreen.attachChild(resultsSprite);
		MainActivity.mScene.setBackground(background);
		MainActivity.mScene.setBackgroundEnabled(true);

	}

	public static void destroyMenu() {
		scene.unregisterTouchArea(beginGameSprite);
		scene.detachChild(beginGameSprite);
	}
}
