package com.example.mycitrusgame.levels;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;

import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.example.mycitrusgame.MainActivity;

public class SecondLevel extends Levels {
	
	Vector2 ballPosition;
	float ballAngle;
	Entity layer;
	static float angel = 0.0f;
	
	PhysicsConnector physicsConnectorForBall;
	PhysicsConnector physicsConnectorForTarget;
	PhysicsConnector physicsConnectorForEnemy1;
	PhysicsConnector physicsConnectorForEnemy2;
	PhysicsConnector physicsConnectorForEnemy3;
	PhysicsConnector physicsConnectorForEnemy4;
	PhysicsConnector physicsConnectorForUpperSprite;
	PhysicsConnector physicsConnectorForDownSprite;
	PhysicsConnector physicsConnectorForMassBody;
	
	static Body uniqueBodyForUpperBody;
	static Body uniqueBodyForDownBody;
	Body ballBody2;
	Body RevoluteJointBodyA;
	Body RevoluteJointBodyB;
	Body targetBody;
	Body enemyBody1;
	Body enemyBody2;
	Body enemyBody3;
	Body enemyBody4;
	Body uniqueBody;
	boolean destroy;
	boolean isTouched = false;
	
	Scene mScene;

	public SecondLevel(Scene scene) {
		this.mScene = scene;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {
		destroy = false;
		layer = new Entity();
		Log.i("build","2 level");
		final Sprite ball = new Sprite(211f, 631f, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite grassGroundLevel = new Sprite(0f, 0f,
				MainActivity.grassGroundTRGoundLevel2TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite upperDiskSprite = new Sprite(470f, 200f,
				MainActivity.secondLevelUpperSpinnerTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite downDiskSprite = new Sprite(470f, 375f,
				MainActivity.secondLevelDownSpinnerTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite target = new Sprite(1156f, 180f, MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite enemy1 = new Sprite(77f, 235f, MainActivity.enemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite enemy2 = new Sprite(135f, 41f, MainActivity.enemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite enemy3 = new Sprite(1167f, 450f, MainActivity.enemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite enemy4 = new Sprite(1030f, 623f, MainActivity.enemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		final short CATEGORYBIT_DEFAULT = 1;
		final short CATEGORYBIT_DISK_CENTER = 2;
		final short CATEGORYBIT_BALL = 4;
		final short MASKBITS_DISK_CENTER = CATEGORYBIT_DEFAULT
				+ CATEGORYBIT_DISK_CENTER;
		final short MASKBITS_BALL = CATEGORYBIT_DEFAULT + CATEGORYBIT_BALL;
		final FixtureDef DISK_CENTER_FIXTURE_DEF = PhysicsFactory
				.createFixtureDef(1, 0.5f, 0.5f, false,
						CATEGORYBIT_DISK_CENTER, MASKBITS_DISK_CENTER,
						(short) 0);
		final FixtureDef BALL_FIXTURE_DEF = PhysicsFactory.createFixtureDef(1,
				0.0f, 0.5f, false, CATEGORYBIT_BALL, MASKBITS_BALL, (short) 0);
		final FixtureDef DISK_FIXTURE_DEF = PhysicsFactory.createFixtureDef(5f,
				0.5f, 1f);
		final FixtureDef COMMON_FIXTURE_DEF = PhysicsFactory.createFixtureDef(
				1f, 0f, 0f);
		
		// /////////////////////////////////////////////////////////////////////
		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		enemyBody1 = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, enemy1, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		enemyBody2 = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, enemy2, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		enemyBody3 = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, enemy3, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		enemyBody4 = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, enemy4, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// /////////////////////////////////////////////////////////////////////
		List<Vector2> UniqueBodyVertices = new ArrayList<Vector2>();
		UniqueBodyVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] {

				new Vector2(0f, 720f), new Vector2(0f, 0f),
						new Vector2(1280f, 0f), new Vector2(1280f, 720f),
						new Vector2(212f, 720f), new Vector2(211f, 670f),
						new Vector2(349f, 590f), new Vector2(523f, 461f),
						new Vector2(549f, 480f), new Vector2(607f, 495f),
						new Vector2(677f, 488f), new Vector2(707f, 468f),
						new Vector2(763f, 552f), new Vector2(803f, 570f),
						new Vector2(811f, 571f), new Vector2(820f, 584f),
						new Vector2(928f, 654f), new Vector2(1088f, 698f),
						new Vector2(1143f, 662f), new Vector2(1148f, 609f),
						new Vector2(1040f, 568f), new Vector2(929f, 510f),
						new Vector2(773f, 380f), new Vector2(778f, 365f),
						new Vector2(890f, 387f), new Vector2(1143f, 502f),
						new Vector2(1183f, 542f), new Vector2(1260f, 512f),
						new Vector2(1253f, 433f), new Vector2(1233f, 376f),
						new Vector2(986f, 318f), new Vector2(755f, 252f),
						new Vector2(747f, 250f), new Vector2(741f, 235f),
						new Vector2(795f, 229f), new Vector2(1019f, 233f),
						new Vector2(1178f, 249f), new Vector2(1235f, 218f),
						new Vector2(1207f, 147f), new Vector2(1047f, 123f),
						new Vector2(853f, 123f), new Vector2(710f, 142f),
						new Vector2(660f, 179f), new Vector2(629f, 178f),
						new Vector2(568f, 191f), new Vector2(545f, 200f),
						new Vector2(414f, 120f), new Vector2(368f, 105f),
						new Vector2(332f, 82f), new Vector2(200f, 38f),
						new Vector2(125f, 25f), new Vector2(58f, 49f),
						new Vector2(86f, 94f), new Vector2(176f, 153f),
						new Vector2(301f, 212f), new Vector2(460f, 276f),
						new Vector2(470f, 280f), new Vector2(471f, 291f),
						new Vector2(324f, 271f), new Vector2(150f, 193f),
						new Vector2(69f, 174f), new Vector2(26f, 208f),
						new Vector2(42f, 280f), new Vector2(131f, 336f),
						new Vector2(296f, 387f), new Vector2(414f, 370f),
						new Vector2(455f, 379f), new Vector2(454f, 384f),
						new Vector2(269f, 455f), new Vector2(178f, 512f),
						new Vector2(120f, 565f), new Vector2(138f, 638f),
						new Vector2(207f, 671f)

				}));

		for (int i = 0; i < UniqueBodyVertices.size(); i++) {
			UniqueBodyVertices.set(i,
					new Vector2(UniqueBodyVertices.get(i).x - 640,
							UniqueBodyVertices.get(i).y - 360));
		}
		ball.setPosition(211f, 600f);

		List<Vector2> UniqueBodyVerticesTriangulated = new EarClippingTriangulator()
				.computeTriangles(UniqueBodyVertices);
		float[] MeshTriangles = new float[UniqueBodyVerticesTriangulated.size() * 3];
		for (int i = 0; i < UniqueBodyVerticesTriangulated.size(); i++) {
			MeshTriangles[i * 3] = UniqueBodyVerticesTriangulated.get(i).x;
			UniqueBodyVerticesTriangulated.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		uniqueBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundLevel,
				UniqueBodyVerticesTriangulated, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// ////////////////////////////////////////////////////
		ballBody2 = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, BALL_FIXTURE_DEF);
		// /////////////////////////////////////////////////
		List<Vector2> downBodyVertices = new ArrayList<Vector2>();
		downBodyVertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(6f, 0f), new Vector2(292f, 0f),
				new Vector2(267f, 50f), new Vector2(226f, 89f),
				new Vector2(185f, 104f), new Vector2(151f, 108f),

				new Vector2(91f, 96f), new Vector2(61f, 78f),
				new Vector2(33f, 53f), new Vector2(14f, 22f)

		}));
		for (int i = 0; i < downBodyVertices.size(); i++) {
			downBodyVertices.set(i, new Vector2(
					downBodyVertices.get(i).x - 150,
					downBodyVertices.get(i).y - 55));
		}
		List<Vector2> UniqueBodyVerticesTriangulatedForDownBody = new EarClippingTriangulator()
				.computeTriangles(downBodyVertices);
		float[] MeshTrianglesForDownrBody = new float[UniqueBodyVerticesTriangulatedForDownBody
				.size() * 3];
		for (int i = 0; i < UniqueBodyVerticesTriangulatedForDownBody.size(); i++) {
			MeshTrianglesForDownrBody[i * 3] = UniqueBodyVerticesTriangulatedForDownBody
					.get(i).x;
			MeshTrianglesForDownrBody[i * 3 + 1] = UniqueBodyVerticesTriangulatedForDownBody
					.get(i).y;
			UniqueBodyVerticesTriangulatedForDownBody.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		uniqueBodyForDownBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, downDiskSprite,
				UniqueBodyVerticesTriangulatedForDownBody,
				BodyType.DynamicBody, DISK_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////
		List<Vector2> upperBodyVertices = new ArrayList<Vector2>();
		upperBodyVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { new Vector2(5f, 110f),
						new Vector2(21f, 74f), new Vector2(54f, 35f),
						new Vector2(98f, 10f), new Vector2(138f, 2f),
						new Vector2(160f, 2f),

						new Vector2(209f, 14f), new Vector2(249f, 40f),
						new Vector2(274f, 78f), new Vector2(287f, 92f),
						new Vector2(293f, 110f)

				}));
		for (int i = 0; i < upperBodyVertices.size(); i++) {
			upperBodyVertices.set(i, new Vector2(
					upperBodyVertices.get(i).x - 150,
					upperBodyVertices.get(i).y - 55));
		}
		List<Vector2> UniqueBodyVerticesTriangulatedForUpperBody = new EarClippingTriangulator()
				.computeTriangles(upperBodyVertices);
		float[] MeshTrianglesForUpperBody = new float[UniqueBodyVerticesTriangulatedForUpperBody
				.size() * 3];
		for (int i = 0; i < UniqueBodyVerticesTriangulatedForUpperBody.size(); i++) {
			MeshTrianglesForUpperBody[i * 3] = UniqueBodyVerticesTriangulatedForUpperBody
					.get(i).x;
			MeshTrianglesForUpperBody[i * 3 + 1] = UniqueBodyVerticesTriangulatedForUpperBody
					.get(i).y;
			UniqueBodyVerticesTriangulatedForUpperBody.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		uniqueBodyForUpperBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, upperDiskSprite,
				UniqueBodyVerticesTriangulatedForUpperBody,
				BodyType.DynamicBody, DISK_FIXTURE_DEF);
		// /////////////////////////////////////////////////////////////////////////////////

		Rectangle RevoluteJointRectA = new Rectangle(615f, 330f, 10f, 10f,
				((BaseGameActivity) MainActivity.context).getEngine()
						.getVertexBufferObjectManager());
		RevoluteJointRectA.setColor(0f, 0f, 0.65f);
		RevoluteJointRectA.setAlpha(0.0f);
		RevoluteJointBodyA = PhysicsFactory.createBoxBody(
				MainActivity.mPhysicsWorld, RevoluteJointRectA,
				BodyType.KinematicBody, DISK_CENTER_FIXTURE_DEF);

		final RevoluteJointDef revoluteJointDefUpperBody = new RevoluteJointDef();
		revoluteJointDefUpperBody.initialize(RevoluteJointBodyA,
				uniqueBodyForUpperBody, RevoluteJointBodyA.getWorldCenter());
		revoluteJointDefUpperBody.collideConnected = false;
		revoluteJointDefUpperBody.enableMotor = true;
		revoluteJointDefUpperBody.maxMotorTorque = 3000f;
		revoluteJointDefUpperBody.motorSpeed = 1.0f;

		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(
						RevoluteJointRectA, RevoluteJointBodyA));
		MainActivity.mPhysicsWorld.createJoint(revoluteJointDefUpperBody);

		final RevoluteJointDef revoluteJointDefDownBody = new RevoluteJointDef();
		revoluteJointDefDownBody.initialize(RevoluteJointBodyA,
				uniqueBodyForDownBody, RevoluteJointBodyA.getWorldCenter());
		revoluteJointDefDownBody.collideConnected = false;
		revoluteJointDefDownBody.enableMotor = true;
		revoluteJointDefDownBody.maxMotorTorque = 3000f;
		revoluteJointDefDownBody.motorSpeed = 1.0f;

		MainActivity.mPhysicsWorld.createJoint(revoluteJointDefDownBody);
		// /////////////////////////////////////////////////////////////////////
		final WeldJointDef weldJointDef = new WeldJointDef();
		weldJointDef.initialize(uniqueBodyForUpperBody, uniqueBodyForDownBody,
				uniqueBodyForUpperBody.getWorldCenter());
		weldJointDef.collideConnected = true;
		MainActivity.mPhysicsWorld.createJoint(weldJointDef);
		// /////////////////////////////////////////////////////////////////////

		physicsConnectorForBall = new PhysicsConnector(ball, ballBody2);
		physicsConnectorForTarget = new PhysicsConnector(target, targetBody);
		physicsConnectorForEnemy1 = new PhysicsConnector(enemy1, enemyBody1);
		physicsConnectorForEnemy2 = new PhysicsConnector(enemy2, enemyBody2);
		physicsConnectorForEnemy3 = new PhysicsConnector(enemy3, enemyBody3);
		physicsConnectorForEnemy4 = new PhysicsConnector(enemy4, enemyBody4);
		physicsConnectorForUpperSprite = new PhysicsConnector(upperDiskSprite,
				uniqueBodyForUpperBody);
		physicsConnectorForDownSprite = new PhysicsConnector(downDiskSprite,
				uniqueBodyForDownBody);
		physicsConnectorForMassBody = new PhysicsConnector(grassGroundLevel,
				uniqueBody);
	
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForUpperSprite);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForDownSprite);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForMassBody);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForBall);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForTarget);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy1);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy2);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy3);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy4);
		
		ballPosition = ballBody2.getWorldCenter();
		ballAngle = ballBody2.getAngle();
		
		layer.attachChild(target);
		layer.attachChild(enemy1);
		layer.attachChild(enemy2);
		layer.attachChild(enemy3);
		layer.attachChild(enemy4);
		layer.attachChild(grassGroundLevel);
		layer.attachChild(upperDiskSprite);
		layer.attachChild(downDiskSprite);
		layer.attachChild(RevoluteJointRectA);
		layer.attachChild(ball);
		
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				if (isTouched) {
					ballBody2.setTransform(ballPosition, ballAngle);
					isTouched = false;
				}
				if (destroy) {
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForBall);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForTarget);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy1);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy2);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy3);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy4);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForUpperSprite);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForDownSprite);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForMassBody);
					MainActivity.mPhysicsWorld.clearPhysicsConnectors();
					MainActivity.mPhysicsWorld.destroyBody(targetBody);
					MainActivity.mPhysicsWorld.destroyBody(ballBody2);
					MainActivity.mPhysicsWorld.destroyBody(RevoluteJointBodyA);
					MainActivity.mPhysicsWorld.destroyBody(uniqueBody);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody1);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody2);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody3);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody4);
					MainActivity.mPhysicsWorld
							.destroyBody(uniqueBodyForUpperBody);
					MainActivity.mPhysicsWorld
							.destroyBody(uniqueBodyForDownBody);

				}
			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {

				if (contact.isTouching()) {
					if (areBodiesContacted(ballBody2, targetBody, contact)) {
						isTouched = true;
						MainActivity
								.setLayer(MainActivity.levelSelector, layer);
						destroyGame();
					}
					if (areBodiesContacted(ballBody2, enemyBody1, contact)) {

						isTouched = true;
						ball.setPosition(211f, 600f);

					}
					if (areBodiesContacted(ballBody2, enemyBody2, contact)) {

						isTouched = true;
						ball.setPosition(211f, 600f);

					}
					if (areBodiesContacted(ballBody2, enemyBody3, contact)) {

						isTouched = true;
						ball.setPosition(211f, 600f);

					}
					if (areBodiesContacted(ballBody2, enemyBody4, contact)) {

						isTouched = true;
						ball.setPosition(211f, 600f);

					}
				}
			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}
		}

		);
		layer.setVisible(true);

		this.mScene.attachChild(layer);
	}

	@Override
	public void destroyGame() {

		destroy = true;
		MainActivity.mPhysicsWorld.reset();
		MainActivity.engine.setScene(MainActivity.mScene);
	}

	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}
}
