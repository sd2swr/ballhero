package com.example.mycitrusgame.levels;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.example.mycitrusgame.MainActivity;

public class NineLevel extends Levels {
	Entity layer;
	Sprite grassGround;
	Sprite enemy;
	Sprite target;
	Sprite ball;
	Vector2 ballPosition;
	float ballAngle;
	PhysicsConnector physicsConnectorForBall;
	PhysicsConnector physicsConnectorForEnemy;
	PhysicsConnector physicsConnectorForTarget;
	Boolean isTouched = false;
	Body ballBody;
	Body targetBody;
	Body enemyBody;
	Body grassGroundBody;
	Body RevoluteJointBodyA;
	Scene mScene;
	boolean destroy;

	public NineLevel(Scene Scene) {
		this.mScene = Scene;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {
		destroy = false;
		layer = new Entity();

		grassGround = new Sprite(0f, 0f, MainActivity.ninthLevelTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy = new Sprite(462f, 173f, MainActivity.enemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		target = new Sprite(640f - 64, 360f - 64, MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		ball = new Sprite(387f, 457f, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final FixtureDef COMMON_FIXTURE_DEF = PhysicsFactory.createFixtureDef(
				0.5f, 0.5f, 0.5f);
		
		
		ballBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, COMMON_FIXTURE_DEF);
		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		enemyBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				enemy, BodyType.DynamicBody, COMMON_FIXTURE_DEF);
	
		ballPosition = ballBody.getWorldCenter();
		ballAngle =  ballBody.getAngle();
		
		// /////////////////////////////////////////////////
		List<Vector2> firstGroundVertices = new ArrayList<Vector2>();
		firstGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { 
						new Vector2(800f, 720f),
						new Vector2(0f, 720f),
						new Vector2(0f, 0f),
						new Vector2(1280f, 0f),
						new Vector2(1280f, 720f),
						new Vector2(819f, 720f),
						new Vector2(800f, 593f),
						new Vector2(915f, 510f),
						new Vector2(984f, 361f),
						new Vector2(973f, 204f),
						new Vector2(880f, 89f),
						new Vector2(739f, 56f),
						new Vector2(575f, 41f),
						new Vector2(438f, 89f),
						new Vector2(356f, 202f),
						new Vector2(312f, 377f),
						new Vector2(359f, 504f),
						new Vector2(433f, 591f),
						new Vector2(561f, 615f),
						new Vector2(790f, 602f),
	
				}));
		for (int i = 0; i < firstGroundVertices.size(); i++) {
			firstGroundVertices.set(i,
					new Vector2(firstGroundVertices.get(i).x - 640,
							firstGroundVertices.get(i).y - 360));
		}
		List<Vector2> VerticesTriangulatedForFirstGround = new EarClippingTriangulator()
				.computeTriangles(firstGroundVertices);
		float[] MeshTrianglesForFirstGround = new float[VerticesTriangulatedForFirstGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForFirstGround.size(); i++) {
			MeshTrianglesForFirstGround[i * 3] = VerticesTriangulatedForFirstGround
					.get(i).x;
			MeshTrianglesForFirstGround[i * 3 + 1] = VerticesTriangulatedForFirstGround
					.get(i).y;
			VerticesTriangulatedForFirstGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGround,
				VerticesTriangulatedForFirstGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		Rectangle RevoluteJointRectA = new Rectangle(640,
				360, 10f, 10f,
				((BaseGameActivity) MainActivity.context).getEngine()
						.getVertexBufferObjectManager());
		RevoluteJointRectA.setColor(0f, 0f, 0.65f);
		RevoluteJointRectA.setAlpha(0.0f);
		RevoluteJointBodyA = PhysicsFactory.createBoxBody(
				MainActivity.mPhysicsWorld, RevoluteJointRectA,
				BodyType.KinematicBody, COMMON_FIXTURE_DEF);

		final RevoluteJointDef revoluteJointDefUpperBody = new RevoluteJointDef();
		revoluteJointDefUpperBody.initialize(RevoluteJointBodyA, enemyBody,
				RevoluteJointBodyA.getWorldCenter());
		revoluteJointDefUpperBody.collideConnected = false;
		revoluteJointDefUpperBody.enableMotor = true;
		revoluteJointDefUpperBody.maxMotorTorque = 3000f;
		revoluteJointDefUpperBody.motorSpeed = 2.0f;

		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(
						RevoluteJointRectA, RevoluteJointBodyA));
		MainActivity.mPhysicsWorld.createJoint(revoluteJointDefUpperBody);

		// /////////////////////////////////////////////////////////////////////
		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {

				if (contact.isTouching()) {
					if (areBodiesContacted(ballBody, targetBody, contact)) {
						isTouched = true;
						MainActivity
								.setLayer(MainActivity.levelSelector, layer);
						destroyGame();
					}

				}
			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}
		}

		);
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				 if(destroy)
				 {
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForTarget);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForBall);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForEnemy);
						MainActivity.mPhysicsWorld.clearPhysicsConnectors();
						MainActivity.mPhysicsWorld.destroyBody(targetBody);
						MainActivity.mPhysicsWorld.destroyBody(ballBody);
						MainActivity.mPhysicsWorld.destroyBody(enemyBody);
						MainActivity.mPhysicsWorld.destroyBody(grassGroundBody);
						MainActivity.mPhysicsWorld.destroyBody(RevoluteJointBodyA);
				 }
				if (ball.collidesWith(enemy)) {
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
				}

			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		physicsConnectorForEnemy = new PhysicsConnector(enemy, enemyBody);
		physicsConnectorForBall = new PhysicsConnector(ball, ballBody);
		physicsConnectorForTarget = new PhysicsConnector(target, targetBody);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForBall);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForTarget);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy);
		layer.attachChild(RevoluteJointRectA);
		layer.attachChild(ball);
		layer.attachChild(enemy);
		layer.attachChild(target);
		layer.attachChild(grassGround);
		layer.setVisible(true);
		this.mScene.attachChild(layer);
	}

	@Override
	public void destroyGame() {
		destroy = true;
		MainActivity.mPhysicsWorld.reset();
		MainActivity.engine.setScene(MainActivity.mScene);

	}

	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}

}
