package com.example.mycitrusgame.levels;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.engine.Engine;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.example.mycitrusgame.EndGameScreen;
import com.example.mycitrusgame.MainActivity;

public class FirstLevel extends Levels {
	Entity layer;

	ButtonSprite resetButton;
	public Font fontDefault32Bold;
	int applesCollected;
	Vector2 ballPosition;
	float ballAngle;
	public static Sound mButtonClickSound;
	public Text countingText;
	long startTime;
	Sprite gate;
	static Sprite ball;
	Sprite target;
	Sprite bg;
	Sprite grassGroundFirstLevel;
	Sprite grassGroundSecondLevel;
	Sprite grassGroundThirdLevel;
	Sprite grassGroundFourthevel;
	Sprite grassGroundGroundLevel;
	Sprite apple1;
	Sprite apple2;
	Sprite apple3;
	float EndingTimer = 60f;
	private ScaleModifier ballInMod;
	private ScaleModifier ballOutMod;

	PhysicsConnector physicsConnectorFoBall;
	PhysicsConnector physicsConnectorForGate;
	PhysicsConnector physicsConnectorFoTarget;

	Body gateBody;
	static Body ballBody;
	Body roofWallBody;
	Body targetBody;
	Body grassGroundGroundLevelBody;
	Body grassGroundFourthLevelBody;
	Body grassGroundThirdLevelBody;
	Body grassGroundSecondLevelBody;
	Body rightWallBody;
	Body leftWallBody;
	Body grassGroundFirstLevelBody;

	short MASKBITS_AREA;
	short MASKBITS_BALL;
	short CATEGORYBIT_BALL;
	FixtureDef WALL_FIXTURE_DEF;
	FixtureDef BALL_FIXTURE_DEF;
	Scene mScene;

	boolean isTouched;

	public FirstLevel(Scene mScene) {
		this.mScene = mScene;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {
		MainActivity.isIngame = true;
		MainActivity.isCleaned = false;
		isDestroy = false;
		isTouched = false;
		applesCollected = 0;
		layer = new Entity();
		isBallChanged = false;
		fontDefault32Bold = FontFactory.create(
				MainActivity.engine.getFontManager(),
				MainActivity.engine.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 32f, true,
				Color.BLACK_ARGB_PACKED_INT);

		fontDefault32Bold.load();
		startTime = System.currentTimeMillis();
		IEntityModifierListener moveXListener = new IEntityModifierListener() {
			@Override
			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {
				MainActivity.isModFinished = false;
			}

			@Override
			public void onModifierFinished(IModifier<IEntity> pModifier,
					IEntity pItem) {

				MainActivity.isModFinished = true;
				pItem.unregisterEntityModifier((IEntityModifier) pModifier);
				pModifier.reset();
			}
		};

		ballInMod = new ScaleModifier(2f, 1f, 0.2f, moveXListener);
		ballOutMod = new ScaleModifier(2f, 0.2f, 1f, moveXListener);
		isBallSmall = false;
		final short CATEGORYBIT_DEFAULT = 1;
		final short CATEGORYBIT_AREA = 2;
		CATEGORYBIT_BALL = 4;
		MASKBITS_AREA = CATEGORYBIT_DEFAULT + CATEGORYBIT_AREA;
		MASKBITS_BALL = (short) (CATEGORYBIT_DEFAULT + CATEGORYBIT_BALL);
		BALL_FIXTURE_DEF = PhysicsFactory.createFixtureDef(1, 0.0f, 0.5f);
		WALL_FIXTURE_DEF = PhysicsFactory.createFixtureDef(1, 0.0f, 0.5f,
				false, CATEGORYBIT_BALL, MASKBITS_BALL, (short) 0);
		final FixtureDef AREA_FIXTURE_DEF = PhysicsFactory.createFixtureDef(1,
				0.0f, 0.5f, false, CATEGORYBIT_BALL, MASKBITS_AREA, (short) 0);

		loadSounds(MainActivity.engine, MainActivity.context);

		resetButton = new ButtonSprite(100f, 100f, MainActivity.resetButtonTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {

					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
				}

				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}

		};
		resetButton.setAlpha(0.5f);
		gate = new Sprite(1100f, 630f, MainActivity.gateTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundFirstLevel = new Sprite(15f,
				MainActivity.cameraHeight - 630f,
				MainActivity.grassGroundTRFirstLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundSecondLevel = new Sprite(150f,
				MainActivity.cameraHeight - 480f,
				MainActivity.grassGroundTRSecondLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundThirdLevel = new Sprite(15f,
				MainActivity.cameraHeight - 330f,
				MainActivity.grassGroundTRThirdLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundFourthevel = new Sprite(150f,
				MainActivity.cameraHeight - 180f,
				MainActivity.grassGroundTRForthLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundGroundLevel = new Sprite(0f,
				MainActivity.cameraHeight - 30f,
				MainActivity.grassGroundTRGoundLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		apple1 = new Sprite(413f, 16f, MainActivity.appleTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		apple2 = new Sprite(513f, 300f, MainActivity.appleTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		apple3 = new Sprite(598f, 500f, MainActivity.appleTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		grassGroundFirstLevel
				.setPosition(15f, MainActivity.cameraHeight - 630f);
		grassGroundSecondLevel.setPosition(150f,
				MainActivity.cameraHeight - 480f);
		grassGroundThirdLevel
				.setPosition(15f, MainActivity.cameraHeight - 330f);
		grassGroundFourthevel.setPosition(150f,
				MainActivity.cameraHeight - 180f);
		grassGroundGroundLevel.setPosition(0f, MainActivity.cameraHeight - 30f);

		final Rectangle roof = new Rectangle(0f, 0f, 1400f, 8f,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Rectangle left = new Rectangle(6f, 0f, 10f,
				MainActivity.cameraHeight,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Rectangle right = new Rectangle(MainActivity.cameraWidth - 20f,
				0f, 10f, MainActivity.cameraHeight,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		target = new Sprite(1216f, MainActivity.cameraHeight - 94f,
				MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		ball = new Sprite(100, 75, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		bg = new Sprite(0, 0, MainActivity.firstLevelBgTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		ballBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, BALL_FIXTURE_DEF);

		ballPosition = ballBody.getWorldCenter();
		ballAngle = ballBody.getAngle();

		// /////////////////////////////////////////////////
		List<Vector2> yellowVertices = new ArrayList<Vector2>();
		yellowVertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(41f, 193f), new Vector2(98f, 168f),
				new Vector2(100f, 0f), new Vector2(0f, 0f),
				new Vector2(0f, 171f)

		}));
		for (int i = 0; i < yellowVertices.size(); i++) {
			yellowVertices.set(i, new Vector2(yellowVertices.get(i).x - 60,
					yellowVertices.get(i).y - 60));
		}
		List<Vector2> VerticesTriangulatedForYellow = new EarClippingTriangulator()
				.computeTriangles(yellowVertices);
		float[] MeshTrianglesForYellow = new float[VerticesTriangulatedForYellow
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForYellow.size(); i++) {
			MeshTrianglesForYellow[i * 3] = VerticesTriangulatedForYellow
					.get(i).x;
			MeshTrianglesForYellow[i * 3 + 1] = VerticesTriangulatedForYellow
					.get(i).y;
			VerticesTriangulatedForYellow.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		gateBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, gate,
				VerticesTriangulatedForYellow, BodyType.StaticBody,
				AREA_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				WALL_FIXTURE_DEF);
		roofWallBody = PhysicsFactory.createBoxBody(MainActivity.mPhysicsWorld,
				roof, BodyType.StaticBody, WALL_FIXTURE_DEF);
		leftWallBody = PhysicsFactory.createBoxBody(MainActivity.mPhysicsWorld,
				left, BodyType.StaticBody, WALL_FIXTURE_DEF);
		rightWallBody = PhysicsFactory.createBoxBody(
				MainActivity.mPhysicsWorld, right, BodyType.StaticBody,
				WALL_FIXTURE_DEF);
		grassGroundFirstLevelBody = PhysicsFactory.createBoxBody(
				MainActivity.mPhysicsWorld, grassGroundFirstLevel,
				BodyType.StaticBody, WALL_FIXTURE_DEF);
		grassGroundSecondLevelBody = PhysicsFactory.createBoxBody(
				MainActivity.mPhysicsWorld, grassGroundSecondLevel,
				BodyType.StaticBody, WALL_FIXTURE_DEF);
		grassGroundThirdLevelBody = PhysicsFactory.createBoxBody(
				MainActivity.mPhysicsWorld, grassGroundThirdLevel,
				BodyType.StaticBody, WALL_FIXTURE_DEF);
		grassGroundFourthLevelBody = PhysicsFactory.createBoxBody(
				MainActivity.mPhysicsWorld, grassGroundFourthevel,
				BodyType.StaticBody, WALL_FIXTURE_DEF);
		grassGroundGroundLevelBody = PhysicsFactory.createBoxBody(
				MainActivity.mPhysicsWorld, grassGroundGroundLevel,
				BodyType.StaticBody, WALL_FIXTURE_DEF);
		countingText = new Text(1200f, 40f, fontDefault32Bold, "10", 10,
				MainActivity.engine.getVertexBufferObjectManager());
		countingText.setAlpha(0.5f);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(
						grassGroundFirstLevel, grassGroundFirstLevelBody));
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(
						grassGroundSecondLevel, grassGroundSecondLevelBody));
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(
						grassGroundThirdLevel, grassGroundThirdLevelBody));
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(
						grassGroundFourthevel, grassGroundFourthLevelBody));
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(
						grassGroundGroundLevel, grassGroundGroundLevelBody));

		ball.setPosition(70, 20);
		bg.setPosition(0, 0);
		layer.attachChild(bg);
		layer.attachChild(ball);
		layer.attachChild(target);
		layer.attachChild(grassGroundFirstLevel);
		layer.attachChild(grassGroundSecondLevel);
		layer.attachChild(grassGroundThirdLevel);
		layer.attachChild(grassGroundFourthevel);
		layer.attachChild(grassGroundGroundLevel);
		layer.attachChild(roof);
		layer.attachChild(left);
		layer.attachChild(right);
		layer.attachChild(resetButton);
		layer.attachChild(countingText);
		layer.attachChild(gate);

		physicsConnectorForGate = new PhysicsConnector(gate, gateBody);
		physicsConnectorFoBall = new PhysicsConnector(ball, ballBody);
		physicsConnectorFoTarget = new PhysicsConnector(target, targetBody);

		mScene.registerTouchArea(resetButton);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForGate);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorFoBall);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorFoTarget);

		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {
				if (contact.isTouching())
					if (areBodiesContacted(ballBody, targetBody, contact)) {
						isTouched = true;

						if (MainActivity.sound) {
							mButtonClickSound.play();
						}
						MainActivity.mMenuThemeMusic.stop();

						MainActivity.mMenuThemeMusic.pause();
						if (MainActivity.lastLevelPassed < 1) {
							MainActivity.lastLevelPassed = 1;
							MainActivity.updateDatabase();
						}
						destroyGame();
					}
			}

			@Override
			public void endContact(Contact contact) {
			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}
		}

		);
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				EndingTimer -= pSecondsElapsed;
				if (EndingTimer <= 0) {
					countingText.setText("0");
					mScene.unregisterUpdateHandler(this);
				} else {
					countingText.setText(String.valueOf(Math.round(EndingTimer)));
				}
				if (!isBallSmall && !isBallChanged
						&& (FirstLevel.ball.getEntityModifierCount() == 0)) {
					for (int i = 0; i < FirstLevel.ball
							.getEntityModifierCount(); i++) {
						FirstLevel.ball.clearEntityModifiers();
					}
					FirstLevel.ball.registerEntityModifier(ballInMod);
					FirstLevel.ball.registerEntityModifier(ballInMod.deepCopy());
					ballBody.getFixtureList().get(0).getShape().setRadius(0.5f);
					ballBody.resetMassData();
					isBallChanged = true;
					isBallSmall = false;
				} else if (!isBallChanged
						&& (FirstLevel.ball.getEntityModifierCount() == 0)) {
					for (int i = 0; i < FirstLevel.ball
							.getEntityModifierCount(); i++) {
						FirstLevel.ball.clearEntityModifiers();
					}
					FirstLevel.ball.registerEntityModifier(ballOutMod);
					FirstLevel.ball.registerEntityModifier(ballOutMod
							.deepCopy());
					ballBody.getFixtureList().get(0).getShape().setRadius(1f);
					ballBody.resetMassData();
					isBallChanged = true;
					isBallSmall = true;
				}
				if (apple1.collidesWith(ball)) {
					apple1.detachSelf();
					applesCollected = 1;
				}
				if (apple2.collidesWith(ball)) {
					apple2.detachSelf();
					applesCollected = 2;
				}
				if (apple3.collidesWith(ball)) {
					apple3.detachSelf();
					// applesCollected = 3;
				}
				if (applesCollected == 3) {

					final float pointsListX[] = { gate.getX(), gate.getX() };

					final float pointsListY[] = { gate.getY(),
							gate.getY() + 200 };
					final int controlPointCount = pointsListX.length;

					org.andengine.entity.modifier.PathModifier.Path path = new Path(
							controlPointCount);

					for (int i = 0; i < controlPointCount; i++) {

						final float positionX = pointsListX[i];
						final float positionY = pointsListY[i];
						path.to(positionX, positionY);
					}
					final float duration = 1;

					PathModifier pathModifier = new PathModifier(duration, path);
					gate.registerEntityModifier(pathModifier);

				}
			}

			@Override
			public void reset() {

			}
		});
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				if (isDestroy) {
					Log.i("destroied", "1 level");
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorFoTarget);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorFoBall);
					MainActivity.mPhysicsWorld.clearPhysicsConnectors();
					MainActivity.mPhysicsWorld.destroyBody(targetBody);
					MainActivity.mPhysicsWorld.destroyBody(ballBody);
					MainActivity.mPhysicsWorld
							.destroyBody(grassGroundFirstLevelBody);
					MainActivity.mPhysicsWorld
							.destroyBody(grassGroundFourthLevelBody);
					MainActivity.mPhysicsWorld
							.destroyBody(grassGroundGroundLevelBody);
					MainActivity.mPhysicsWorld
							.destroyBody(grassGroundSecondLevelBody);
					MainActivity.mPhysicsWorld
							.destroyBody(grassGroundThirdLevelBody);
					MainActivity.mPhysicsWorld.destroyBody(leftWallBody);
					MainActivity.mPhysicsWorld.destroyBody(rightWallBody);
					MainActivity.mPhysicsWorld.destroyBody(roofWallBody);
					MainActivity.isCleaned = true;

				}

			}

			@Override
			public void reset() {
			}
		});
		layer.attachChild(apple3);
		layer.attachChild(apple2);
		layer.attachChild(apple1);
		layer.setVisible(true);
		this.mScene.attachChild(layer);
		MainActivity.currentLevel = this;
		MainActivity.currentScene = this.mScene;
	}

	@Override
	public void destroyGame() {

		if (((System.currentTimeMillis() - startTime) / 1000) < 15) {
			MainActivity.stars[0] = 3;
		} else if (((System.currentTimeMillis() - startTime) / 1000) < 20) {
			MainActivity.stars[0] = 2;
		} else if (((System.currentTimeMillis() - startTime) / 1000) < 30) {
			MainActivity.stars[0] = 1;
		}
		((Activity) MainActivity.context).runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(
						((ContextWrapper) MainActivity.context)
								.getBaseContext(),
						"��� ������� ������ �� "
								+ ((System.currentTimeMillis() - startTime) / 1000)
								+ "��� � �������� " + MainActivity.stars[0]
								+ "�����", Toast.LENGTH_SHORT).show();
			}

		});
		for (int i = 0; i < layer.getChildCount(); i++) {
			layer.getChildByIndex(i).setColor(0.4f, 0.4f, 0.4f);
		}
		MainActivity.pass[0] = true;
		isDestroy = true;

		MainActivity.currentScene.setIgnoreUpdate(true);
		EndGameScreen.buildScreen(mScene);

	}

	public static void loadSounds(Engine engine, Context context) {
		SoundFactory.setAssetBasePath("music/");
		try {
			mButtonClickSound = SoundFactory.createSoundFromAsset(
					engine.getSoundManager(), context, "target_music.mp3");
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		MusicFactory.setAssetBasePath("music/");
		try {

			MainActivity.mMenuThemeMusic = MusicFactory.createMusicFromAsset(
					engine.getMusicManager(), context, "theme_music.mp3");

			MainActivity.mMenuThemeMusic.setLooping(true);
			if (MainActivity.sound) {
				MainActivity.mMenuThemeMusic.play();
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}

}