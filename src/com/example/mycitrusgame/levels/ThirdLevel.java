package com.example.mycitrusgame.levels;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.primitive.DrawMode;
import org.andengine.entity.primitive.Mesh;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;

import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.example.mycitrusgame.MainActivity;

public class ThirdLevel extends Levels {
	Body ballBody;
	Body targetBody;
	Body uniqueBody;
	Body uniqueBody1;
	Body uniqueBody2;
	Body uniqueBody3;
	Body uniqueBody4;

	PhysicsConnector physicsConnectorForBall;
	PhysicsConnector physicsConnectorForTarget;

	Entity layer;
	Scene mScene;

	boolean destroy;
	boolean isTouched;

	Vector2 ballPosition;

	float balAngel;

	public ThirdLevel(Scene scene) {
		this.mScene = scene;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {
		isTouched = false;
		destroy = false;
		layer = new Entity();

		final Sprite ball = new Sprite(300f, 392f, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite grassGroundLevel = new Sprite(0f, 0f,
				MainActivity.grassGroundGroundLevel3TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite grassGroundLevel1 = new Sprite(0f, 0f,
				MainActivity.grassGroundGroundLevel3FirstGroundTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite grassGroundLevel2 = new Sprite(0f, 0f,
				MainActivity.grassGroundGroundLevel3SecondGroundTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite grassGroundLevel3 = new Sprite(0f, 0f,
				MainActivity.grassGroundGroundLevel3ThirdGroundTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite grassGroundLevel4 = new Sprite(0f, 0f,
				MainActivity.grassGroundGroundLevel3FourthGroundTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final Sprite target = new Sprite(1000f, 400f, MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		FixtureDef uniqueBodyFixtureDef = PhysicsFactory.createFixtureDef(1f,
				0.5f, 1f);

		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				uniqueBodyFixtureDef);
		// =============================================
		// ============ground===========================
		// =============================================
		List<Vector2> goundBodyVectors = new ArrayList<Vector2>();
		goundBodyVectors.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(0f, 720f), new Vector2(0f, 0f),
				new Vector2(1280f, 0f), new Vector2(1280f, 720f),
				new Vector2(139f, 720f), new Vector2(131f, 663f),
				new Vector2(478f, 664f), new Vector2(665f, 667f),
				new Vector2(1171f, 653f), new Vector2(1219f, 629f),
				new Vector2(1223f, 540f), new Vector2(1204f, 74f),
				new Vector2(1238f, 69f), new Vector2(391f, 71f),
				new Vector2(97f, 81f), new Vector2(71f, 124f),
				new Vector2(50f, 376f), new Vector2(66f, 582f),
				new Vector2(82f, 642f), new Vector2(122f, 662f), }));
		for (int i = 0; i < goundBodyVectors.size(); i++) {
			goundBodyVectors.set(i, new Vector2(
					goundBodyVectors.get(i).x - 640,
					goundBodyVectors.get(i).y - 360));
		}
		List<Vector2> gorundBodyVectorsTriangulated = new EarClippingTriangulator()
				.computeTriangles(goundBodyVectors);
		float[] MeshTriangles = new float[gorundBodyVectorsTriangulated.size() * 3];
		for (int i = 0; i < gorundBodyVectorsTriangulated.size(); i++) {
			MeshTriangles[i * 3] = gorundBodyVectorsTriangulated.get(i).x;
			MeshTriangles[i * 3 + 1] = gorundBodyVectorsTriangulated.get(i).y;
			gorundBodyVectorsTriangulated.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundLevel.setPosition(0f, 0f);
		grassGroundLevel1.setPosition(162f, 443f);
		grassGroundLevel2.setPosition(309f, 174f);
		grassGroundLevel3.setPosition(614f, 473f);
		grassGroundLevel4.setPosition(770f, 227f);
		Mesh goundBodyMesh = new Mesh(0f, 0f, MeshTriangles,
				gorundBodyVectorsTriangulated.size(), DrawMode.TRIANGLES,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		uniqueBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundLevel,
				gorundBodyVectorsTriangulated, BodyType.StaticBody,
				uniqueBodyFixtureDef);

		// ----------------------------------
		// =============================================
		// ============firstgr===========================
		// =============================================
		List<Vector2> goundBodyVectors1 = new ArrayList<Vector2>();
		goundBodyVectors1.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] {

				new Vector2(96f, 152f), new Vector2(251f, 133f),
						new Vector2(304f, 100f), new Vector2(341f, 55f),
						new Vector2(338f, 34f), new Vector2(296f, 36f),
						new Vector2(272f, 55f), new Vector2(242f, 103f),
						new Vector2(130f, 118f), new Vector2(87f, 79f),
						new Vector2(62f, 25f), new Vector2(35f, 18f),
						new Vector2(29f, 35f), new Vector2(48f, 112f), }));
		for (int i = 0; i < goundBodyVectors1.size(); i++) {
			goundBodyVectors1.set(i, new Vector2(
					goundBodyVectors1.get(i).x - 185,
					goundBodyVectors1.get(i).y - 87));
		}
		List<Vector2> gorundBodyVectorsTriangulated1 = new EarClippingTriangulator()
				.computeTriangles(goundBodyVectors1);
		float[] MeshTriangles1 = new float[gorundBodyVectorsTriangulated1
				.size() * 3];
		for (int i = 0; i < gorundBodyVectorsTriangulated1.size(); i++) {
			MeshTriangles1[i * 3] = gorundBodyVectorsTriangulated1.get(i).x;
			MeshTriangles1[i * 3 + 1] = gorundBodyVectorsTriangulated1.get(i).y;
			gorundBodyVectorsTriangulated1.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		uniqueBody1 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundLevel1,
				gorundBodyVectorsTriangulated1, BodyType.StaticBody,
				uniqueBodyFixtureDef);

		// ----------------------------------

		// ----------------------------------
		// =============================================
		// ============secondtgr===========================
		// =============================================
		List<Vector2> goundBodyVectors2 = new ArrayList<Vector2>();
		goundBodyVectors2.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] {

				new Vector2(48f, 120f), new Vector2(100f, 84f),
						new Vector2(168f, 78f), new Vector2(256f, 139f),
						new Vector2(304f, 150f), new Vector2(342f, 123f),
						new Vector2(316f, 70f), new Vector2(255f, 37f),
						new Vector2(168f, 25f), new Vector2(164f, 30f),
						new Vector2(92f, 38f), new Vector2(30f, 100f), }));
		for (int i = 0; i < goundBodyVectors2.size(); i++) {
			goundBodyVectors2.set(i, new Vector2(
					goundBodyVectors2.get(i).x - 185,
					goundBodyVectors2.get(i).y - 87));
		}
		List<Vector2> gorundBodyVectorsTriangulated2 = new EarClippingTriangulator()
				.computeTriangles(goundBodyVectors2);
		float[] MeshTriangles2 = new float[gorundBodyVectorsTriangulated2
				.size() * 3];
		for (int i = 0; i < gorundBodyVectorsTriangulated2.size(); i++) {
			MeshTriangles2[i * 3] = gorundBodyVectorsTriangulated2.get(i).x;
			MeshTriangles2[i * 3 + 1] = gorundBodyVectorsTriangulated2.get(i).y;
			gorundBodyVectorsTriangulated2.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		uniqueBody2 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundLevel2,
				gorundBodyVectorsTriangulated2, BodyType.StaticBody,
				uniqueBodyFixtureDef);

		// ----------------------------------
		// =============================================
		// ============thirdgr===========================
		// =============================================
		List<Vector2> goundBodyVectors3 = new ArrayList<Vector2>();
		goundBodyVectors3.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] {

				new Vector2(73f, 134f), new Vector2(202f, 164f),
						new Vector2(266f, 156f), new Vector2(316f, 131f),
						new Vector2(348f, 56f), new Vector2(338f, 30f),
						new Vector2(323f, 25f), new Vector2(308f, 27f),
						new Vector2(263f, 109f), new Vector2(202f, 122f),
						new Vector2(136f, 104f), new Vector2(78f, 27f),
						new Vector2(41f, 12f), new Vector2(30f, 46f),
						new Vector2(38f, 89f),

				}));
		for (int i = 0; i < goundBodyVectors3.size(); i++) {
			goundBodyVectors3.set(i, new Vector2(
					goundBodyVectors3.get(i).x - 185,
					goundBodyVectors3.get(i).y - 87));
		}
		List<Vector2> gorundBodyVectorsTriangulated3 = new EarClippingTriangulator()
				.computeTriangles(goundBodyVectors3);
		float[] MeshTriangles3 = new float[gorundBodyVectorsTriangulated3
				.size() * 3];
		for (int i = 0; i < gorundBodyVectorsTriangulated3.size(); i++) {
			MeshTriangles3[i * 3] = gorundBodyVectorsTriangulated3.get(i).x;
			MeshTriangles3[i * 3 + 1] = gorundBodyVectorsTriangulated3.get(i).y;
			gorundBodyVectorsTriangulated3.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		uniqueBody3 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundLevel3,
				gorundBodyVectorsTriangulated3, BodyType.StaticBody,
				uniqueBodyFixtureDef);

		// ----------------------------------
		// =============================================
		// ============fourthdgr===========================
		// =============================================
		List<Vector2> goundBodyVectors4 = new ArrayList<Vector2>();
		goundBodyVectors4.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] {

				new Vector2(24f, 126f), new Vector2(45f, 92f),
						new Vector2(100f, 74f), new Vector2(180f, 75f),
						new Vector2(226f, 106f), new Vector2(248f, 129f),
						new Vector2(261f, 161f), new Vector2(291f, 161f),
						new Vector2(325f, 148f), new Vector2(325f, 110f),
						new Vector2(295f, 67f), new Vector2(229f, 34f),
						new Vector2(146f, 18f), new Vector2(50f, 48f),
						new Vector2(8f, 88f), new Vector2(8f, 126f), }));
		for (int i = 0; i < goundBodyVectors4.size(); i++) {
			goundBodyVectors4.set(i, new Vector2(
					goundBodyVectors4.get(i).x - 185,
					goundBodyVectors4.get(i).y - 87));
		}
		List<Vector2> gorundBodyVectorsTriangulated4 = new EarClippingTriangulator()
				.computeTriangles(goundBodyVectors4);
		float[] MeshTriangles4 = new float[gorundBodyVectorsTriangulated4
				.size() * 3];
		for (int i = 0; i < gorundBodyVectorsTriangulated4.size(); i++) {
			MeshTriangles4[i * 3] = gorundBodyVectorsTriangulated4.get(i).x;
			MeshTriangles4[i * 3 + 1] = gorundBodyVectorsTriangulated4.get(i).y;
			gorundBodyVectorsTriangulated4.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		uniqueBody4 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundLevel4,
				gorundBodyVectorsTriangulated4, BodyType.StaticBody,
				uniqueBodyFixtureDef);

		// ----------------------------------
		ballBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, uniqueBodyFixtureDef);
		ballPosition = ballBody.getWorldCenter();
		balAngel = ballBody.getAngle();
		physicsConnectorForBall = new PhysicsConnector(ball, ballBody);
		physicsConnectorForTarget = new PhysicsConnector(target, targetBody);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForBall);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForTarget);
		goundBodyMesh.setColor(1f, 0f, 0f);
		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {

				if (contact.isTouching()) {

					if (areBodiesContacted(ballBody, targetBody, contact)) {
						Log.i("contact", "---");
						isTouched = true;
						MainActivity
								.setLayer(MainActivity.levelSelector, layer);
						destroyGame();
					}
					if (areBodiesContacted(ballBody, uniqueBody, contact)) {

						isTouched = true;
						ball.setPosition(572f, 392f);
					}
				}
			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}
		}

		);
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				if (destroy) {
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForBall);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForTarget);
					MainActivity.mPhysicsWorld.clearPhysicsConnectors();
					MainActivity.mPhysicsWorld.destroyBody(targetBody);
					MainActivity.mPhysicsWorld.destroyBody(ballBody);
					MainActivity.mPhysicsWorld.destroyBody(uniqueBody);
					MainActivity.mPhysicsWorld.destroyBody(uniqueBody1);
					MainActivity.mPhysicsWorld.destroyBody(uniqueBody2);
					MainActivity.mPhysicsWorld.destroyBody(uniqueBody3);
					MainActivity.mPhysicsWorld.destroyBody(uniqueBody4);
				}
			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				if (isTouched) {
					ballBody.setTransform(ballPosition, balAngel);
					isTouched = false;
				}
			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		layer.attachChild(target);
		layer.attachChild(grassGroundLevel4);
		layer.attachChild(grassGroundLevel3);
		layer.attachChild(grassGroundLevel2);
		layer.attachChild(grassGroundLevel);
		layer.attachChild(grassGroundLevel1);

		layer.attachChild(ball);
		layer.setVisible(true);
		this.mScene.attachChild(layer);
	}

	@Override
	public void destroyGame() {
		destroy = true;
		MainActivity.mPhysicsWorld.reset();
		MainActivity.engine.setScene(MainActivity.mScene);
	}

	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}
}