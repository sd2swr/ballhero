package com.example.mycitrusgame.levels;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.example.mycitrusgame.MainActivity;

public class TenthLevel extends Levels {
	Entity layer;
	Sprite grassGround;
	Sprite enemy1;
	Sprite enemy2;
	Sprite enemy4;
	Sprite enemy3;
	Sprite enemy5;
	Sprite target;
	Sprite ball;
	Vector2 ballPosition;
	float ballAngle;
	PhysicsConnector physicsConnectorForBall;
	PhysicsConnector physicsConnectorForEnemy1;
	PhysicsConnector physicsConnectorForEnemy2;
	PhysicsConnector physicsConnectorForEnemy3;
	PhysicsConnector physicsConnectorForEnemy4;
	PhysicsConnector physicsConnectorForEnemy5;
	PhysicsConnector physicsConnectorForTarget;
	Boolean isTouched = false;
	Body ballBody;
	Body targetBody;
	Body enemyBody1;
	Body enemyBody2;
	Body enemyBody3;
	Body enemyBody4;
	Body enemyBody5;
	Body grassGroundBody;
	Scene mScene;
	boolean destroy;

	public TenthLevel(Scene Scene) {
		this.mScene = Scene;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {
		destroy = false;
		layer = new Entity();

		grassGround = new Sprite(0f, 0f, MainActivity.ninthLevelTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy1 = new Sprite(973f, 373f, MainActivity.tenthLevelEnemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy2 = new Sprite(973f, 373f, MainActivity.tenthLevelEnemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy3 = new Sprite(973f, 373f, MainActivity.tenthLevelEnemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy4 = new Sprite(973f, 373f, MainActivity.tenthLevelEnemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy5 = new Sprite(973f, 373f, MainActivity.tenthLevelEnemyTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		target = new Sprite(1056, 496, MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		ball = new Sprite(174f, 500f, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		final FixtureDef COMMON_FIXTURE_DEF = PhysicsFactory.createFixtureDef(
				0.5f, 0.5f, 0.5f);

		ballBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, COMMON_FIXTURE_DEF);
		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);

		ballPosition = ballBody.getWorldCenter();
		ballAngle = ballBody.getAngle();

		// /////////////////////////////////////////////////
		List<Vector2> firstGroundVertices = new ArrayList<Vector2>();
		firstGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { new Vector2(415f, 720f),
						new Vector2(0f, 720f), new Vector2(0f, 0f),
						new Vector2(1280f, 0f), new Vector2(1280f, 720f),
						new Vector2(444f, 720f), new Vector2(443f, 603f),
						new Vector2(441f, 361f), new Vector2(890f, 357f),
						new Vector2(907f, 564f), new Vector2(1217f, 560f),
						new Vector2(1202f, 334f), new Vector2(1010f, 335f),
						new Vector2(999f, 256f), new Vector2(666f, 251f),
						new Vector2(666f, 201f), new Vector2(900f, 189f),
						new Vector2(908f, 34f), new Vector2(329f, 50f),
						new Vector2(357f, 211f), new Vector2(476f, 206f),
						new Vector2(481f, 262f), new Vector2(301f, 262f),
						new Vector2(301f, 351f), new Vector2(81f, 349f),
						new Vector2(68f, 607f), new Vector2(442f, 608f),
						new Vector2(0f, 0f), new Vector2(0f, 0f),
						new Vector2(0f, 0f), new Vector2(0f, 0f),
						new Vector2(0f, 0f),

				}));
		for (int i = 0; i < firstGroundVertices.size(); i++) {
			firstGroundVertices.set(i,
					new Vector2(firstGroundVertices.get(i).x - 640,
							firstGroundVertices.get(i).y - 360));
		}
		List<Vector2> VerticesTriangulatedForFirstGround = new EarClippingTriangulator()
				.computeTriangles(firstGroundVertices);
		float[] MeshTrianglesForFirstGround = new float[VerticesTriangulatedForFirstGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForFirstGround.size(); i++) {
			MeshTrianglesForFirstGround[i * 3] = VerticesTriangulatedForFirstGround
					.get(i).x;
			MeshTrianglesForFirstGround[i * 3 + 1] = VerticesTriangulatedForFirstGround
					.get(i).y;
			VerticesTriangulatedForFirstGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGround,
				VerticesTriangulatedForFirstGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////
		// /////////////////////////////////////////////////
		List<Vector2> enemyVertices = new ArrayList<Vector2>();
		enemyVertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(0f, 0f), new Vector2(35f, 0f),
				new Vector2(35f, 35f), new Vector2(0f, 35f), }));
		for (int i = 0; i < enemyVertices.size(); i++) {
			enemyVertices.set(i, new Vector2(enemyVertices.get(i).x - (34 / 2),
					enemyVertices.get(i).y - (34 / 2)));
		}
		List<Vector2> VerticesTriangulatedForEnemy = new EarClippingTriangulator()
				.computeTriangles(enemyVertices);
		float[] MeshTrianglesForEnemy = new float[VerticesTriangulatedForEnemy
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForEnemy.size(); i++) {
			MeshTrianglesForEnemy[i * 3] = VerticesTriangulatedForEnemy.get(i).x;
			MeshTrianglesForEnemy[i * 3 + 1] = VerticesTriangulatedForEnemy
					.get(i).y;
			VerticesTriangulatedForEnemy.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		enemyBody1 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, enemy1,
				VerticesTriangulatedForEnemy, BodyType.DynamicBody,
				COMMON_FIXTURE_DEF);
		enemyBody2 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, enemy2,
				VerticesTriangulatedForEnemy, BodyType.DynamicBody,
				COMMON_FIXTURE_DEF);
		enemyBody3 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, enemy3,
				VerticesTriangulatedForEnemy, BodyType.DynamicBody,
				COMMON_FIXTURE_DEF);
		enemyBody4 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, enemy4,
				VerticesTriangulatedForEnemy, BodyType.DynamicBody,
				COMMON_FIXTURE_DEF);
		enemyBody5 = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, enemy5,
				VerticesTriangulatedForEnemy, BodyType.DynamicBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////
		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {

				if (contact.isTouching()) {
					if (areBodiesContacted(ballBody, targetBody, contact)) {
						isTouched = true;
						MainActivity
								.setLayer(MainActivity.levelSelector, layer);
						destroyGame();
					}

				}
			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}
		}

		);
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				if (destroy) {
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForTarget);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForBall);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy1);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy2);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy3);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy4);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForEnemy5);
					MainActivity.mPhysicsWorld.clearPhysicsConnectors();
					MainActivity.mPhysicsWorld.destroyBody(targetBody);
					MainActivity.mPhysicsWorld.destroyBody(ballBody);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody1);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody2);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody3);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody4);
					MainActivity.mPhysicsWorld.destroyBody(enemyBody5);
					MainActivity.mPhysicsWorld.destroyBody(grassGroundBody);
				}
				if (ball.collidesWith(enemy1)) {
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
				}

				if (ball.collidesWith(enemy2)) {
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
				}

				if (ball.collidesWith(enemy3)) {
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
				}

				if (ball.collidesWith(enemy4)) {
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
				}

				if (ball.collidesWith(enemy5)) {
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
				}

				;
			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		physicsConnectorForEnemy1 = new PhysicsConnector(enemy1, enemyBody1);
		physicsConnectorForEnemy2 = new PhysicsConnector(enemy2, enemyBody2);
		physicsConnectorForEnemy3 = new PhysicsConnector(enemy3, enemyBody3);
		physicsConnectorForEnemy4 = new PhysicsConnector(enemy4, enemyBody4);
		physicsConnectorForEnemy5 = new PhysicsConnector(enemy5, enemyBody5);
		physicsConnectorForBall = new PhysicsConnector(ball, ballBody);
		physicsConnectorForTarget = new PhysicsConnector(target, targetBody);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForBall);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForTarget);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy1);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy2);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy3);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy4);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForEnemy5);
		layer.attachChild(ball);
		layer.attachChild(enemy1);
		layer.attachChild(enemy2);
		layer.attachChild(enemy3);
		layer.attachChild(enemy4);
		layer.attachChild(enemy5);
		layer.attachChild(target);
		layer.attachChild(grassGround);
		layer.setVisible(true);
		this.mScene.attachChild(layer);
	}

	@Override
	public void destroyGame() {
		destroy = true;
		MainActivity.mPhysicsWorld.reset();
		MainActivity.engine.setScene(MainActivity.mScene);
	}

	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}

}
