package com.example.mycitrusgame.levels;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.example.mycitrusgame.MainActivity;

public class SixthLevel extends Levels {
	Entity layer;
	Sprite grassGroundFirstLevel;
	Sprite grassGroundSecondLevel;
	Sprite grassGroundThirdLevel;
	Sprite target;
	Sprite ball;
	PhysicsConnector physicsConnectorForBall;
	PhysicsConnector physicsConnectorForGround;
	PhysicsConnector physicsConnectorForTarget;
	Boolean isTouched = false;
	Body ballBody;
	Body targetBody;
	Body grassGroundThirdLevelBody;
	Body grassGroundFirstLevelBody;
	Body grassGroundSecondLevelBody;
	Scene mScene;
	boolean one;
	boolean destroy;
	public SixthLevel(Scene mScene) {
		this.mScene = mScene;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {
		layer = new Entity();
		one = true;
		grassGroundFirstLevel = new Sprite(498f, 14f,
				MainActivity.grassGroundSixthLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundSecondLevel = new Sprite(60f, 263f,
				MainActivity.grassGroundSixthLevel2TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundThirdLevel = new Sprite(1010f, 238f,
				MainActivity.grassGroundSixthLevel3TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		target = new Sprite(1104, 305f, MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		ball = new Sprite(196f, 316f, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		final FixtureDef COMMON_FIXTURE_DEF = PhysicsFactory.createFixtureDef(
				0.5f, 0.5f, 0.5f);

		ballBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, COMMON_FIXTURE_DEF);
		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		///////////////////////////////////////////////////
		List<Vector2> firstGroundVertices = new ArrayList<Vector2>();
		firstGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { new Vector2(70f, 680f),
						new Vector2(50f, 682f), new Vector2(42f, 662f),
						new Vector2(67f, 623f), new Vector2(106f, 605f),
						new Vector2(117f, 184f), new Vector2(115f, 428f),
						new Vector2(67f, 383f), new Vector2(78f, 336f),
						new Vector2(111f, 303f), new Vector2(94f, 97f),
						new Vector2(54f, 65f), new Vector2(59f, 39f),
						new Vector2(69f, 39f), new Vector2(103f, 80f),
						new Vector2(142f, 104f), new Vector2(189f, 110f),
						new Vector2(219f, 90f), new Vector2(238f, 48f),
						new Vector2(257f, 46f), new Vector2(262f, 65f),
						new Vector2(241f, 104f), new Vector2(201f, 120f),
						new Vector2(190f, 247f), new Vector2(192f, 300f),
						new Vector2(240f, 343f), new Vector2(233f, 389f),
						new Vector2(195f, 425f), new Vector2(194f, 607f),
						new Vector2(210f, 631f), new Vector2(252f, 663f),
						new Vector2(247f, 690f), new Vector2(237f, 690f),
						new Vector2(202f, 647f), new Vector2(164f, 624f),
						new Vector2(119f, 618f), new Vector2(88f, 638f),
						new Vector2(68f, 680f) }));
		for (int i = 0; i < firstGroundVertices.size(); i++) {
			firstGroundVertices.set(i,
					new Vector2(firstGroundVertices.get(i).x - 150,
							firstGroundVertices.get(i).y - 360));
		}
		List<Vector2> VerticesTriangulatedForFirstGround = new EarClippingTriangulator()
				.computeTriangles(firstGroundVertices);
		float[] MeshTrianglesForFirstGround = new float[VerticesTriangulatedForFirstGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForFirstGround.size(); i++) {
			MeshTrianglesForFirstGround[i * 3] = VerticesTriangulatedForFirstGround
					.get(i).x;
			MeshTrianglesForFirstGround[i * 3 + 1] = VerticesTriangulatedForFirstGround
					.get(i).y;
			VerticesTriangulatedForFirstGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundFirstLevelBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundFirstLevel,
				VerticesTriangulatedForFirstGround, BodyType.KinematicBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		// /////////////////////////////////////////////////
		List<Vector2> secondGroundVertices = new ArrayList<Vector2>();
		secondGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { new Vector2(58f, 56f),
						new Vector2(88f, 60f), new Vector2(107f, 96f),
						new Vector2(99f, 151f), new Vector2(130f, 195f),
						new Vector2(175f, 188f), new Vector2(225f, 120f),
						new Vector2(248f, 120f), new Vector2(262f, 150f),
						new Vector2(240f, 207f), new Vector2(190f, 242f),
						new Vector2(93f, 224f), new Vector2(49f, 163f),
						new Vector2(36f, 123f), new Vector2(39f, 75f) }));
		for (int i = 0; i < secondGroundVertices.size(); i++) {
			secondGroundVertices.set(i,
					new Vector2(secondGroundVertices.get(i).x - 150,
							secondGroundVertices.get(i).y - 150));
		}
		List<Vector2> VerticesTriangulatedForSecondGround = new EarClippingTriangulator()
				.computeTriangles(secondGroundVertices);
		float[] MeshTrianglesForSecondGround = new float[VerticesTriangulatedForSecondGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForSecondGround.size(); i++) {
			MeshTrianglesForSecondGround[i * 3] = VerticesTriangulatedForSecondGround
					.get(i).x;
			MeshTrianglesForSecondGround[i * 3 + 1] = VerticesTriangulatedForSecondGround
					.get(i).y;
			VerticesTriangulatedForSecondGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundSecondLevelBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundSecondLevel,
				VerticesTriangulatedForSecondGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////
		// /////////////////////////////////////////////////
		List<Vector2> thirdGroundVertices = new ArrayList<Vector2>();
		thirdGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { new Vector2(142f,204f),
						new Vector2(198f,193f),
						new Vector2(223f,154f),
						new Vector2(197f,79f),
						new Vector2(233f,63f),
						new Vector2(261f,106f),
						new Vector2(248f,201f),
						new Vector2(200f,240f),
						new Vector2(97f,229f),
						new Vector2(50f,178f),
						new Vector2(65f,135f),
						new Vector2(80f,129f),
						new Vector2(108f,175f),
						new Vector2(138f,202f),
				}));
		for (int i = 0; i < thirdGroundVertices.size(); i++) {
			thirdGroundVertices.set(i,
					new Vector2(thirdGroundVertices.get(i).x - 150,
							thirdGroundVertices.get(i).y - 150));
		}
		List<Vector2> VerticesTriangulatedForThirdGround = new EarClippingTriangulator()
				.computeTriangles(thirdGroundVertices);
		float[] MeshTrianglesForThirdGround = new float[VerticesTriangulatedForThirdGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForThirdGround.size(); i++) {
			MeshTrianglesForThirdGround[i * 3] = VerticesTriangulatedForThirdGround
					.get(i).x;
			MeshTrianglesForThirdGround[i * 3 + 1] = VerticesTriangulatedForThirdGround
					.get(i).y;
			VerticesTriangulatedForThirdGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundThirdLevelBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundThirdLevel,
				VerticesTriangulatedForThirdGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////1
		layer.registerUpdateHandler(new IUpdateHandler() {
			 @Override
			 public void onUpdate(float pSecondsElapsed) {
				 if(destroy)
				 {
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForTarget);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForBall);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForGround);
						MainActivity.mPhysicsWorld.clearPhysicsConnectors();
						MainActivity.mPhysicsWorld.destroyBody(targetBody);
						MainActivity.mPhysicsWorld.destroyBody(ballBody);
						MainActivity.mPhysicsWorld.destroyBody(grassGroundFirstLevelBody);
						MainActivity.mPhysicsWorld.destroyBody(grassGroundSecondLevelBody);
						MainActivity.mPhysicsWorld.destroyBody(grassGroundThirdLevelBody);
				 }
				 if(one)
				 {
					 one = false;
				 grassGroundFirstLevelBody.setAngularVelocity(1.0f);
				 }
			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub
				
			}
		});
		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {
				if (contact.isTouching())
					if (areBodiesContacted(ballBody, targetBody, contact)) {
						isTouched = true;
						MainActivity
								.setLayer(MainActivity.levelSelector, layer);
						destroyGame();
					}
			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}
		}

		);
		physicsConnectorForBall = (new PhysicsConnector(ball, ballBody));
		physicsConnectorForTarget = new PhysicsConnector(target,targetBody);
		physicsConnectorForGround = new PhysicsConnector(grassGroundFirstLevel,grassGroundFirstLevelBody); 
		MainActivity.mPhysicsWorld
		.registerPhysicsConnector(physicsConnectorForBall);
		MainActivity.mPhysicsWorld
		.registerPhysicsConnector(physicsConnectorForTarget);
		MainActivity.mPhysicsWorld
		.registerPhysicsConnector(physicsConnectorForGround);
		layer.attachChild(ball);
		layer.attachChild(target);
		layer.attachChild(grassGroundFirstLevel);
		layer.attachChild(grassGroundSecondLevel);
		layer.attachChild(grassGroundThirdLevel);
		layer.setVisible(true);
		this.mScene.attachChild(layer);
	}

	@Override
	public void destroyGame() {
		destroy = true;
		MainActivity.mPhysicsWorld.reset();
		MainActivity.engine.setScene(MainActivity.mScene);

	}

	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}
}
