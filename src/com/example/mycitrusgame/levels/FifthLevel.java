package com.example.mycitrusgame.levels;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;

import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.example.mycitrusgame.MainActivity;

public class FifthLevel extends Levels{
	Entity layer;
	Sprite grassGroundFirstLevel;
	Sprite grassGroundSecondLevel;
	Sprite pinkArea;
	Sprite yellowArea;
	Sprite target;
	Sprite ball;
	PhysicsConnector physicsConnectorForBall;
	PhysicsConnector physicsConnectorForDisk;
	PhysicsConnector physicsConnectorForTarget;
	Boolean isTouched = false;
	Body ballBody;
	Body targetBody;
	Body yellowBody;
	Body grassGroundFirstLevelBody;
	Body grassGroundSecondLevelBody;
	Body pinkBody;
	Scene mScene;
	float ballElst;
	boolean destroy;
	boolean isYellow;
	boolean isPink;
	FixtureDef BALL_FIXTURE_DEF ;
	short	MASKBITS_AREA;
	short   MASKBITS_BALL;
	short CATEGORYBIT_BALL;
	public FifthLevel(Scene mScene) {
		this.mScene = mScene;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {

		isYellow =  false;
		isPink =  false;
		ballElst = 0.0f;
		destroy = false;
		layer = new Entity();
		
		final short CATEGORYBIT_DEFAULT = 1;
		final short CATEGORYBIT_AREA = 2;
		CATEGORYBIT_BALL = 4;
	    MASKBITS_AREA = CATEGORYBIT_DEFAULT
				+ CATEGORYBIT_AREA;
	    MASKBITS_BALL = (short) (CATEGORYBIT_DEFAULT + CATEGORYBIT_BALL);
		
		BALL_FIXTURE_DEF = PhysicsFactory.createFixtureDef(1,
				0.0f, 0.5f, false, CATEGORYBIT_BALL, MASKBITS_BALL, (short) 0);
		final FixtureDef AREA_FIXTURE_DEF = PhysicsFactory.createFixtureDef(1,
				0.0f, 0.5f, false, CATEGORYBIT_BALL, MASKBITS_AREA, (short) 0);
		grassGroundFirstLevel = new Sprite(0f, 0f,
				MainActivity.grassGroundTRFifthLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundSecondLevel = new Sprite(980f, 580f,
				MainActivity.grassGroundTRFifthLevel2TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		yellowArea = new Sprite(434f, 559f, MainActivity.yellowFifthLevelTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		pinkArea = new Sprite(252f, 556f, MainActivity.pinkFifthLevelTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		target = new Sprite(1063f, 518f, MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		ball = new Sprite(100f, 560f, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		yellowArea.setAlpha(0.5f);
		pinkArea.setAlpha(0.5f);
		
		final FixtureDef COMMON_FIXTURE_DEF = PhysicsFactory.createFixtureDef(
				0.5f, 0.5f, 0.5f);

		ballBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, BALL_FIXTURE_DEF);
		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// /////////////////////////////////////////////////
		List<Vector2> firstGroundVertices = new ArrayList<Vector2>();
		firstGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] {
						new Vector2(662f, 718f),
						new Vector2(664f, 674f), new Vector2(20f, 674f),
						new Vector2(25f, 557f), new Vector2(652f, 562f),
						new Vector2(0f, 14f), new Vector2(0f,720f)
				}));
		for (int i = 0; i < firstGroundVertices.size(); i++) {
			firstGroundVertices.set(i,
					new Vector2(firstGroundVertices.get(i).x - 350,
							firstGroundVertices.get(i).y - 360));
		}
		List<Vector2> VerticesTriangulatedForFirstGround = new EarClippingTriangulator()
				.computeTriangles(firstGroundVertices);
		float[] MeshTrianglesForFirstGround = new float[VerticesTriangulatedForFirstGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForFirstGround.size(); i++) {
			MeshTrianglesForFirstGround[i * 3] = VerticesTriangulatedForFirstGround
					.get(i).x;
			MeshTrianglesForFirstGround[i * 3 + 1] = VerticesTriangulatedForFirstGround
					.get(i).y;
			VerticesTriangulatedForFirstGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundFirstLevelBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundFirstLevel,
				VerticesTriangulatedForFirstGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		// /////////////////////////////////////////////////
		List<Vector2> secondGroundVertices = new ArrayList<Vector2>();
		secondGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { 
						new Vector2(25f, 198f),
						new Vector2(13f, 192f),
						new Vector2(18f, 72f),
						new Vector2(45f, 80f),
						new Vector2(60f, 108f),
						new Vector2(108f, 133f),
						new Vector2(166f, 133f),
						new Vector2(231f, 88f),
						new Vector2(270f, 81f),
						new Vector2(298f, 83f),
						new Vector2(300f, 200f),
				}));
		for (int i = 0; i < secondGroundVertices.size(); i++) {
			secondGroundVertices.set(i,
					new Vector2(secondGroundVertices.get(i).x - 150,
							secondGroundVertices.get(i).y - 100));
		}
		List<Vector2> VerticesTriangulatedForSecondGround = new EarClippingTriangulator()
				.computeTriangles(secondGroundVertices);
		float[] MeshTrianglesForSecondGround = new float[VerticesTriangulatedForSecondGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForSecondGround.size(); i++) {
			MeshTrianglesForSecondGround[i * 3] = VerticesTriangulatedForSecondGround
					.get(i).x;
			MeshTrianglesForSecondGround[i * 3 + 1] = VerticesTriangulatedForSecondGround
					.get(i).y;
			VerticesTriangulatedForSecondGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundSecondLevelBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundSecondLevel,
				VerticesTriangulatedForSecondGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////
		// /////////////////////////////////////////////////
		List<Vector2> yellowVertices = new ArrayList<Vector2>();
		yellowVertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(15f, 117f),
				new Vector2(21f, 5f),
				new Vector2(102f, 6f),
				new Vector2(106f, 115f)
				
		}));
		for (int i = 0; i < yellowVertices.size(); i++) {
			yellowVertices.set(i, new Vector2(yellowVertices.get(i).x - 60,
					yellowVertices.get(i).y - 60));
		}
		List<Vector2> VerticesTriangulatedForYellow = new EarClippingTriangulator()
				.computeTriangles(yellowVertices);
		float[] MeshTrianglesForYellow = new float[VerticesTriangulatedForYellow
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForYellow.size(); i++) {
			MeshTrianglesForYellow[i * 3] = VerticesTriangulatedForYellow.get(i).x;
			MeshTrianglesForYellow[i * 3 + 1] = VerticesTriangulatedForYellow
					.get(i).y;
			VerticesTriangulatedForYellow.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		yellowBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, yellowArea, VerticesTriangulatedForYellow,
				BodyType.StaticBody, AREA_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		// /////////////////////////////////////////////////
		List<Vector2> pinkVertices = new ArrayList<Vector2>();
		pinkVertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(8f, 108f),
				new Vector2(11f, 5f),
				new Vector2(105f, 4f),
				new Vector2(110f, 115f),
				
		}));
		for (int i = 0; i < pinkVertices.size(); i++) {
			pinkVertices.set(i, new Vector2(pinkVertices.get(i).x - 60,
					pinkVertices.get(i).y - 60));
		}
		List<Vector2> VerticesTriangulatedForPink = new EarClippingTriangulator()
				.computeTriangles(pinkVertices);
		float[] MeshTrianglesForPink = new float[VerticesTriangulatedForPink
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForPink.size(); i++) {
			MeshTrianglesForPink[i * 3] = VerticesTriangulatedForPink.get(i).x;
			MeshTrianglesForPink[i * 3 + 1] = VerticesTriangulatedForPink
					.get(i).y;
			VerticesTriangulatedForPink.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		pinkBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, pinkArea, VerticesTriangulatedForPink,
				BodyType.StaticBody, AREA_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		MainActivity.mPhysicsWorld
		.registerPhysicsConnector(new PhysicsConnector(yellowArea, yellowBody));
		
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(ball, ballBody));
		
		layer.registerUpdateHandler(new IUpdateHandler() {
			 @Override
			 public void onUpdate(float pSecondsElapsed) {
				 if(pinkArea.collidesWith(ball))
				 {
						Log.i("Contact", "pink");
					 if(ballElst < 1.0f)
					 {
						 ballElst +=0.05f;
						 ballBody.getFixtureList().get(0).setRestitution(ballElst);
					 }
				 }
				 if(yellowArea.collidesWith(ball))
				 {
						Log.i("Contact", "yellow");
					 if(ballElst > 0.0f)
					 {
						 ballElst -=0.05f;
						 ballBody.getFixtureList().get(0).setRestitution(ballElst);
					 }
				 }
				 
			 }

			@Override
			public void reset() {
				// TODO Auto-generated method stub
				
			}
		});
		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {

				if (contact.isTouching()) {
					if (areBodiesContacted(ballBody, targetBody, contact)) {
						isTouched = true;
						MainActivity
								.setLayer(MainActivity.levelSelector, layer);
						destroyGame();
					}
					if (areBodiesContacted(ballBody, pinkBody, contact)) 
					{
						Log.i("Contact", "pink");
						isPink =  true;
					}
					else
					{
						isPink =  false;
					}
					if (areBodiesContacted(ballBody, yellowBody, contact)) 
					{
						isYellow =  true;
					}
					else
					{
						isYellow =  false;	
					}
				}
			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}
		}

		);
		layer.attachChild(ball);
		layer.attachChild(target);
		layer.attachChild(grassGroundFirstLevel);
		layer.attachChild(grassGroundSecondLevel);
		layer.attachChild(yellowArea);
		layer.attachChild(pinkArea);
		layer.setVisible(true);
		layer.registerUpdateHandler(new IUpdateHandler() {
			 @Override
			 public void onUpdate(float pSecondsElapsed) {
				 if(destroy)
				 {
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForTarget);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForBall);
						MainActivity.mPhysicsWorld.clearPhysicsConnectors();
						MainActivity.mPhysicsWorld.destroyBody(targetBody);
						MainActivity.mPhysicsWorld.destroyBody(ballBody);
						MainActivity.mPhysicsWorld.destroyBody(grassGroundFirstLevelBody);
						MainActivity.mPhysicsWorld.destroyBody(grassGroundSecondLevelBody);
						MainActivity.mPhysicsWorld.destroyBody(yellowBody);
						MainActivity.mPhysicsWorld.destroyBody(pinkBody);
				 }
				 
			 }

			@Override
			public void reset() {
				// TODO Auto-generated method stub
				
			}
		});
		this.mScene.attachChild(layer);
	}

	@Override
	public void destroyGame() {
		destroy = true;
		MainActivity.mPhysicsWorld.reset();
		MainActivity.engine.setScene(MainActivity.mScene);
		
	}

	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}
}
