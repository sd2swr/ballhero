package com.example.mycitrusgame.levels;

import org.andengine.entity.scene.Scene;

public abstract class Levels extends Scene {
	public boolean isDestroy;
	public boolean isBallSmall;
	public boolean isBallChanged;

	public abstract void buildGame();

	public abstract void destroyGame();
}
