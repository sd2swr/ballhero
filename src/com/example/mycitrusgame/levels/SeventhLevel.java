package com.example.mycitrusgame.levels;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;

import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.example.mycitrusgame.MainActivity;

public class SeventhLevel extends Levels {
	Entity layer;
	Sprite grassGroundFirstLevel;
	Sprite enemy1;
	Sprite enemy2;
	Sprite enemy3;
	Sprite target;
	Sprite ball;
	boolean up1;
	boolean up2;
	boolean up3;
	boolean down1;
	boolean down2;
	boolean down3;
	boolean destroy;
	Vector2 ballPosition;
	float ballAngle;
	PhysicsConnector physicsConnectorForBall;
	PhysicsConnector physicsConnectorForTarget;
	PhysicsConnector physicsConnectorForEnemy1;
	PhysicsConnector physicsConnectorForEnemy2;
	PhysicsConnector physicsConnectorForEnemy3;
	Boolean isTouched = false;
	Body grassGroundFirstLevelBody;
	Body ballBody;
	Body targetBody;
	Body enemy1Body;
	Body enemy2Body;
	Body enemy3Body;
	Scene mScene;

	public SeventhLevel(Scene mScene) {
		this.mScene = mScene;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {
		layer = new Entity();
		 up1 = true;
		 up2 = true;
		 up3 = true;
		 down1 = false;
		 down2 = false;
		 down3 = false;
		grassGroundFirstLevel = new Sprite(0f, 0f, MainActivity.seventhLevelTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy1 = new Sprite(450f, 50f, MainActivity.enemyLevel7TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy2 = new Sprite(575f, 50f, MainActivity.enemyLevel7TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		enemy3 = new Sprite(750f, 50f, MainActivity.enemyLevel7TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		target = new Sprite(1104, 305f, MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		ball = new Sprite(200f, 213f, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		final FixtureDef COMMON_FIXTURE_DEF = PhysicsFactory.createFixtureDef(
				0.5f, 0.5f, 0.5f);

		ballBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, COMMON_FIXTURE_DEF);
		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		ballPosition = ballBody.getWorldCenter();
		ballAngle = ballBody.getAngle();
		// /////////////////////////////////////////////////
		List<Vector2> firstGroundVertices = new ArrayList<Vector2>();
		firstGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { 
						 new Vector2(61f, 25f),
						 new Vector2(1229f, 30f),
						 new Vector2(1238f, 680f),
						 new Vector2(34f, 684f),
						 new Vector2(41f, 48f),
						 new Vector2(0f, 58f),
						 new Vector2(0f, 720f),
						 new Vector2(1280f, 720f),
						 new Vector2(1280f, 0f),
						 new Vector2(61f, 0f)
					}));
		for (int i = 0; i < firstGroundVertices.size(); i++) {
			firstGroundVertices.set(i,
					new Vector2(firstGroundVertices.get(i).x - 640,
							firstGroundVertices.get(i).y - 360));
		}
		List<Vector2> VerticesTriangulatedForFirstGround = new EarClippingTriangulator()
				.computeTriangles(firstGroundVertices);
		float[] MeshTrianglesForFirstGround = new float[VerticesTriangulatedForFirstGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForFirstGround.size(); i++) {
			MeshTrianglesForFirstGround[i * 3] = VerticesTriangulatedForFirstGround
					.get(i).x;
			MeshTrianglesForFirstGround[i * 3 + 1] = VerticesTriangulatedForFirstGround
					.get(i).y;
			VerticesTriangulatedForFirstGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundFirstLevelBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundFirstLevel,
				VerticesTriangulatedForFirstGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		// /////////////////////////////////////////////////
		List<Vector2> enemy1Vertices = new ArrayList<Vector2>();
		enemy1Vertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(4f, 178f), new Vector2(4f, 22f),
				new Vector2(40f, 19f), new Vector2(44f, 178f), }));
		for (int i = 0; i < enemy1Vertices.size(); i++) {
			enemy1Vertices.set(i, new Vector2(enemy1Vertices.get(i).x - 25,
					enemy1Vertices.get(i).y - 100));
		}
		List<Vector2> VerticesTriangulatedForEnemy1 = new EarClippingTriangulator()
				.computeTriangles(enemy1Vertices);
		float[] MeshTrianglesForEnemy1 = new float[VerticesTriangulatedForEnemy1
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForEnemy1.size(); i++) {
			MeshTrianglesForEnemy1[i * 3] = VerticesTriangulatedForEnemy1
					.get(i).x;
			MeshTrianglesForEnemy1[i * 3 + 1] = VerticesTriangulatedForEnemy1
					.get(i).y;
			VerticesTriangulatedForEnemy1.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		enemy1Body = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, enemy1,
				VerticesTriangulatedForEnemy1, BodyType.KinematicBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		// /////////////////////////////////////////////////
		List<Vector2> enemy2Vertices = new ArrayList<Vector2>();
		enemy2Vertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(4f, 178f), new Vector2(4f, 22f),
				new Vector2(40f, 19f), new Vector2(44f, 178f), }));
		for (int i = 0; i < enemy2Vertices.size(); i++) {
			enemy2Vertices.set(i, new Vector2(enemy2Vertices.get(i).x - 25,
					enemy2Vertices.get(i).y - 100));
		}
		List<Vector2> VerticesTriangulatedForEnemy2 = new EarClippingTriangulator()
				.computeTriangles(enemy2Vertices);
		float[] MeshTrianglesForEnemy2 = new float[VerticesTriangulatedForEnemy2
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForEnemy2.size(); i++) {
			MeshTrianglesForEnemy2[i * 3] = VerticesTriangulatedForEnemy2
					.get(i).x;
			MeshTrianglesForEnemy2[i * 3 + 1] = VerticesTriangulatedForEnemy2
					.get(i).y;
			VerticesTriangulatedForEnemy2.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		enemy2Body = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, enemy2,
				VerticesTriangulatedForEnemy2, BodyType.KinematicBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		// /////////////////////////////////////////////////
		List<Vector2> enemy3Vertices = new ArrayList<Vector2>();
		enemy3Vertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(4f, 178f), new Vector2(4f, 22f),
				new Vector2(40f, 19f), new Vector2(44f, 178f), }));
		for (int i = 0; i < enemy3Vertices.size(); i++) {
			enemy3Vertices.set(i, new Vector2(enemy3Vertices.get(i).x - 25,
					enemy3Vertices.get(i).y - 100));
		}
		List<Vector2> VerticesTriangulatedForEnemy3 = new EarClippingTriangulator()
				.computeTriangles(enemy3Vertices);
		float[] MeshTrianglesForEnemy3 = new float[VerticesTriangulatedForEnemy3
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForEnemy3.size(); i++) {
			MeshTrianglesForEnemy3[i * 3] = VerticesTriangulatedForEnemy3
					.get(i).x;
			MeshTrianglesForEnemy3[i * 3 + 1] = VerticesTriangulatedForEnemy3
					.get(i).y;
			VerticesTriangulatedForEnemy3.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		enemy3Body = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, enemy3,
				VerticesTriangulatedForEnemy3, BodyType.KinematicBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				 if(destroy)
				 {
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForTarget);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForBall);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForEnemy1);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForEnemy2);
						MainActivity.mPhysicsWorld.unregisterPhysicsConnector(physicsConnectorForEnemy3);
						MainActivity.mPhysicsWorld.clearPhysicsConnectors();
						MainActivity.mPhysicsWorld.destroyBody(targetBody);
						MainActivity.mPhysicsWorld.destroyBody(ballBody);
						MainActivity.mPhysicsWorld.destroyBody(grassGroundFirstLevelBody);
						MainActivity.mPhysicsWorld.destroyBody(enemy1Body);
						MainActivity.mPhysicsWorld.destroyBody(enemy2Body);
						MainActivity.mPhysicsWorld.destroyBody(enemy3Body);
				 }
				if (ball.collidesWith(enemy1)) {

					ball.setPosition(40f, 213f);
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
						

				}
				if (ball.collidesWith(enemy2)) {
					ball.setPosition(40f, 213f);
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
				}
				if (ball.collidesWith(enemy3)) {
					ball.setPosition(40f, 213f);
					ballBody.setLinearVelocity(0f, 0f);
					ballBody.setTransform(ballPosition, ballAngle);
					
				}
				Log.i("Y1:","" + enemy1Body.getWorldCenter().y );
				Log.i("Y2:","" + enemy2Body.getWorldCenter().y );
				Log.i("Y3:","" + enemy3Body.getWorldCenter().y );
				if (enemy1Body.getWorldCenter().y < 6.0f && up1) {
					Log.i("Y:","aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" );
					enemy1Body.setLinearVelocity(0.0f, 5.0f);
					up1 = false;
					down1 = true;
				}
				if (enemy2Body.getWorldCenter().y < 6.0f && up2) {
					enemy2Body.setLinearVelocity(0.0f, 10.0f);
					up2 = false;
					down2 = true;
				}
				if (enemy3Body.getWorldCenter().y < 6.0f && up3) {
					enemy3Body.setLinearVelocity(0.0f, 15.0f);
					up3 = false;
					down3 = true;
				}

				if (enemy1Body.getWorldCenter().y > 19.0f && down1) {
					enemy1Body.setLinearVelocity(0.0f, -5.0f);
					up1 = true;
					down1 = false;
				}
				if (enemy2Body.getWorldCenter().y > 19.0f && down2) {
					enemy2Body.setLinearVelocity(0.0f, -10.0f);
					up2 = true;
					down2 = false;
				}
				if (enemy3Body.getWorldCenter().y > 19.0f && down3) {
					enemy3Body.setLinearVelocity(0.0f, -15.0f);
					up3 = true;
					down3 = false;
				}

			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {

			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {

				if (contact.isTouching()) {
					if (areBodiesContacted(ballBody, targetBody, contact)) {
						isTouched = true;
						MainActivity
								.setLayer(MainActivity.levelSelector, layer);
						destroyGame();
					}
	
				}
			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}
		}

		);
		physicsConnectorForEnemy1 = new PhysicsConnector(enemy1, enemy1Body);
		physicsConnectorForEnemy2 = new PhysicsConnector(enemy2, enemy2Body);
		physicsConnectorForEnemy3 = new PhysicsConnector(enemy3, enemy3Body);
		MainActivity.mPhysicsWorld
		.registerPhysicsConnector(physicsConnectorForEnemy1);
		MainActivity.mPhysicsWorld
		.registerPhysicsConnector(physicsConnectorForEnemy2);
		MainActivity.mPhysicsWorld
		.registerPhysicsConnector(physicsConnectorForEnemy3);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(ball, ballBody));
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(new PhysicsConnector(
						grassGroundFirstLevel, grassGroundFirstLevelBody));
		layer.attachChild(ball);
		layer.attachChild(target);
		layer.attachChild(grassGroundFirstLevel);
		layer.attachChild(enemy1);
		layer.attachChild(enemy2);
		layer.attachChild(enemy3);
		layer.setVisible(true);
		this.mScene.attachChild(layer);
	}

	@Override
	public void destroyGame() {
		destroy = true;
		MainActivity.mPhysicsWorld.reset();
		MainActivity.engine.setScene(MainActivity.mScene);

	}
	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}
}
