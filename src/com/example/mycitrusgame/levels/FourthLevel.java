package com.example.mycitrusgame.levels;

import java.util.ArrayList;
import java.util.List;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.extension.physics.box2d.util.triangulation.EarClippingTriangulator;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.list.ListUtils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.example.mycitrusgame.MainActivity;

public class FourthLevel extends Levels {
	Entity layer;
	Sprite grassGroundFirstLevel;
	Sprite grassGroundSecondLevel;
	Sprite disk;
	Sprite target;
	Sprite ball;
	PhysicsConnector physicsConnectorForBall;
	PhysicsConnector physicsConnectorForDisk;
	PhysicsConnector physicsConnectorForTarget;
	Boolean isTouched = false;
	Body ballBody;
	Body targetBody;
	Body diskBody;

	Body grassGroundFirstLevelBody;
	Body grassGroundSecondLevelBody;
	Scene mScene;
	boolean destroy;
	boolean isLeft;
	boolean isRight;

	public FourthLevel(Scene mScene) {
		this.mScene = mScene;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildGame() {
		isLeft = true;
		isRight = false;
		destroy = false;
		layer = new Entity();
		grassGroundFirstLevel = new Sprite(0f, 0f,
				MainActivity.grassGroundTRFourthLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		grassGroundSecondLevel = new Sprite(1280f - 500f, 0f,
				MainActivity.grassGroundTRFourthLevel2TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		disk = new Sprite(640f, 400f, MainActivity.diskTRFourthtLevel1TR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		target = new Sprite(1050f, 50f, MainActivity.targetTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());
		ball = new Sprite(120f, 150f, MainActivity.ballTR,
				((BaseGameActivity) MainActivity.context)
						.getVertexBufferObjectManager());

		final FixtureDef COMMON_FIXTURE_DEF = PhysicsFactory.createFixtureDef(
				0.5f, 0.5f, 0.5f);

		ballBody = PhysicsFactory.createCircleBody(MainActivity.mPhysicsWorld,
				ball, BodyType.DynamicBody, COMMON_FIXTURE_DEF);
		targetBody = PhysicsFactory.createCircleBody(
				MainActivity.mPhysicsWorld, target, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// /////////////////////////////////////////////////
		List<Vector2> firstGroundVertices = new ArrayList<Vector2>();
		firstGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { new Vector2(1f, 329f),
						new Vector2(58f, 324f), new Vector2(116f, 312f),
						new Vector2(128f, 292f), new Vector2(127f, 264f),
						new Vector2(93f, 176f), new Vector2(108f, 128f),
						new Vector2(152f, 92f), new Vector2(172f, 87f),
						new Vector2(242f, 95f), new Vector2(265f, 129f),
						new Vector2(264f, 190f), new Vector2(235f, 221f),
						new Vector2(223f, 240f), new Vector2(232f, 266f),
						new Vector2(264f, 295f), new Vector2(304f, 295f),
						new Vector2(307f, 167f), new Vector2(296f, 85f),
						new Vector2(304f, 51f), new Vector2(776f, 2f),
						new Vector2(0f, 0f) }));
		for (int i = 0; i < firstGroundVertices.size(); i++) {
			firstGroundVertices.set(i,
					new Vector2(firstGroundVertices.get(i).x - 210,
							firstGroundVertices.get(i).y - 175));
		}
		List<Vector2> VerticesTriangulatedForFirstGround = new EarClippingTriangulator()
				.computeTriangles(firstGroundVertices);
		float[] MeshTrianglesForFirstGround = new float[VerticesTriangulatedForFirstGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForFirstGround.size(); i++) {
			MeshTrianglesForFirstGround[i * 3] = VerticesTriangulatedForFirstGround
					.get(i).x;
			MeshTrianglesForFirstGround[i * 3 + 1] = VerticesTriangulatedForFirstGround
					.get(i).y;
			VerticesTriangulatedForFirstGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundFirstLevelBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundFirstLevel,
				VerticesTriangulatedForFirstGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		// /////////////////////////////////////////////////
		List<Vector2> secondGroundVertices = new ArrayList<Vector2>();
		secondGroundVertices.addAll((List<Vector2>) ListUtils
				.toList(new Vector2[] { new Vector2(500f, 336f),
						new Vector2(426f, 339f), new Vector2(390f, 294f),
						new Vector2(374f, 210f), new Vector2(397f, 167f),
						new Vector2(411f, 122f), new Vector2(414f, 70f),
						new Vector2(440f, 40f), new Vector2(347f, 37f),
						new Vector2(300f, 46f), new Vector2(278f, 77f),
						new Vector2(270f, 141f), new Vector2(290f, 232f),
						new Vector2(292f, 183f), new Vector2(260f, 320f),
						new Vector2(174f, 324f), new Vector2(112f, 272f),
						new Vector2(101f, 180f), new Vector2(83f, 74f),
						new Vector2(52f, 0f), new Vector2(500f, 0f),

				}));
		for (int i = 0; i < secondGroundVertices.size(); i++) {
			secondGroundVertices.set(i,
					new Vector2(secondGroundVertices.get(i).x - 250,
							secondGroundVertices.get(i).y - 200));
		}
		List<Vector2> VerticesTriangulatedForSecondGround = new EarClippingTriangulator()
				.computeTriangles(secondGroundVertices);
		float[] MeshTrianglesForSecondGround = new float[VerticesTriangulatedForSecondGround
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForSecondGround.size(); i++) {
			MeshTrianglesForSecondGround[i * 3] = VerticesTriangulatedForSecondGround
					.get(i).x;
			MeshTrianglesForSecondGround[i * 3 + 1] = VerticesTriangulatedForSecondGround
					.get(i).y;
			VerticesTriangulatedForSecondGround.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		grassGroundSecondLevelBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, grassGroundSecondLevel,
				VerticesTriangulatedForSecondGround, BodyType.StaticBody,
				COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////
		// /////////////////////////////////////////////////
		List<Vector2> diskVertices = new ArrayList<Vector2>();
		diskVertices.addAll((List<Vector2>) ListUtils.toList(new Vector2[] {
				new Vector2(39f, 27f), new Vector2(23f, 50f),
				new Vector2(19f, 138f), new Vector2(32f, 190f),
				new Vector2(57f, 248f), new Vector2(113f, 272f),
				new Vector2(305f, 280f), new Vector2(380f, 180f),
				new Vector2(370f, 92f), new Vector2(340f, 65f),
				new Vector2(312f, 64f), new Vector2(285f, 55f),
				new Vector2(243f, 63f), new Vector2(233f, 86f),
				new Vector2(303f, 120f), new Vector2(302f, 211f),
				new Vector2(197f, 235f), new Vector2(107f, 211f),
				new Vector2(80f, 129f), new Vector2(115f, 80f),
				new Vector2(76f, 35) }));
		for (int i = 0; i < diskVertices.size(); i++) {
			diskVertices.set(i, new Vector2(diskVertices.get(i).x - 200,
					diskVertices.get(i).y - 150));
		}
		List<Vector2> VerticesTriangulatedForDisk = new EarClippingTriangulator()
				.computeTriangles(diskVertices);
		float[] MeshTrianglesForDisk = new float[VerticesTriangulatedForDisk
				.size() * 3];
		for (int i = 0; i < VerticesTriangulatedForDisk.size(); i++) {
			MeshTrianglesForDisk[i * 3] = VerticesTriangulatedForDisk.get(i).x;
			MeshTrianglesForDisk[i * 3 + 1] = VerticesTriangulatedForDisk
					.get(i).y;
			VerticesTriangulatedForDisk.get(i).mul(
					1 / PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
		}
		diskBody = PhysicsFactory.createTrianglulatedBody(
				MainActivity.mPhysicsWorld, disk, VerticesTriangulatedForDisk,
				BodyType.KinematicBody, COMMON_FIXTURE_DEF);
		// //////////////////////////////////////////////////////////;
		physicsConnectorForDisk = new PhysicsConnector(disk, diskBody);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForDisk);

		physicsConnectorForBall = new PhysicsConnector(ball, ballBody);
		MainActivity.mPhysicsWorld
				.registerPhysicsConnector(physicsConnectorForBall);

		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {


				if (diskBody.getWorldCenter().x < 0.0f && isLeft) {
					isLeft = false;
					isRight = true;
					diskBody.setAngularVelocity(2.0f);
					diskBody.setLinearVelocity(3.0f, 0.0f);

				}
				if (diskBody.getWorldCenter().x > 40.0f && isRight) {
					isLeft = true;
					isRight = false;
					diskBody.setAngularVelocity(2.0f);
					diskBody.setLinearVelocity(-3.0f, 0.0f);
				}

			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		layer.attachChild(ball);
		layer.attachChild(target);
		layer.attachChild(grassGroundFirstLevel);
		layer.attachChild(grassGroundSecondLevel);
		layer.attachChild(disk);
		layer.setVisible(true);
		MainActivity.mPhysicsWorld.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {
				if (contact.isTouching())
					if (areBodiesContacted(ballBody, targetBody, contact)) {
						isTouched = true;
						MainActivity
								.setLayer(MainActivity.levelSelector, layer);
						destroyGame();
					}
			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}
		}

		);
		layer.registerUpdateHandler(new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				if (destroy) {
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForTarget);
					MainActivity.mPhysicsWorld
							.unregisterPhysicsConnector(physicsConnectorForBall);
					MainActivity.mPhysicsWorld.clearPhysicsConnectors();
					MainActivity.mPhysicsWorld.destroyBody(targetBody);
					MainActivity.mPhysicsWorld.destroyBody(ballBody);
					MainActivity.mPhysicsWorld
							.destroyBody(grassGroundFirstLevelBody);
					MainActivity.mPhysicsWorld
							.destroyBody(grassGroundSecondLevelBody);
					MainActivity.mPhysicsWorld.destroyBody(diskBody);

				}

			}

			@Override
			public void reset() {
				// TODO Auto-generated method stub

			}
		});
		this.mScene.attachChild(layer);
	}

	@Override
	public void destroyGame() {

		destroy = true;
		MainActivity.mPhysicsWorld.reset();
		MainActivity.engine.setScene(MainActivity.mScene);
	}

	public boolean areBodiesContacted(Body pBody1, Body pBody2, Contact pContact) {
		if (isTouched) {
			return false;
		}
		if (pContact.getFixtureA().getBody().equals(pBody1)
				|| pContact.getFixtureB().getBody().equals(pBody1))
			if (pContact.getFixtureA().getBody().equals(pBody2)
					|| pContact.getFixtureB().getBody().equals(pBody2))
				return true;
		return false;
	}
}
