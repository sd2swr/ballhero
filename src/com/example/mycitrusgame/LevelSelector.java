package com.example.mycitrusgame;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.extension.physics.box2d.util.Vector2Pool;
import org.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.region.ITextureRegion;

import com.badlogic.gdx.math.Vector2;
import com.example.mycitrusgame.levels.EightLevel;
import com.example.mycitrusgame.levels.FifthLevel;
import com.example.mycitrusgame.levels.FirstLevel;
import com.example.mycitrusgame.levels.FourthLevel;
import com.example.mycitrusgame.levels.NineLevel;
import com.example.mycitrusgame.levels.SecondLevel;
import com.example.mycitrusgame.levels.SeventhLevel;
import com.example.mycitrusgame.levels.SixthLevel;
import com.example.mycitrusgame.levels.TenthLevel;
import com.example.mycitrusgame.levels.ThirdLevel;

public class LevelSelector extends Entity implements IOnSceneTouchListener {

	private final int COLUMNS = 5;
	private final int ROWS = 4;
	private LevelSelector context;

	private final int TILE_DIMENSION = 50;
	private final int TILE_PADDING = 60;
	private float screenY = 0.0f;
	private float screenX = 0.0f;
	private final int mChapter;
	private boolean isMoved;
	private boolean isFirstDown;
	private boolean isMoving;
	private final Scene mScene;
	private final Engine mEngine;
	private float firstX = 0.0f;

	private final int mCameraWidth;
	private final int mCameraHeight;
	private float mInitialX;
	@SuppressWarnings("unused")
	private final float mInitialY;

	private boolean mHidden = false;

	public LevelSelector(final int pChapter, final int pCameraWidth,
			final int pCameraHeight, final Scene pScene, final Engine pEngine) {

		this.mScene = pScene;
		this.mEngine = pEngine;
		this.mChapter = pChapter;
		this.mCameraWidth = pCameraWidth;
		this.mCameraHeight = pCameraHeight;

		final float halfLevelSelectorWidth = ((TILE_DIMENSION * COLUMNS) + TILE_PADDING
				* (COLUMNS - 1)) * 0.5f;
		this.mInitialX = (this.mCameraWidth * 0.5f) - halfLevelSelectorWidth;

		final float halfLevelSelectorHeight = ((TILE_DIMENSION * ROWS) + TILE_PADDING
				* (ROWS - 1)) * 0.5f;
		this.mInitialY = (this.mCameraHeight * 0.5f) + halfLevelSelectorHeight;
	}

	public void createTiles(final ITextureRegion pTextureRegion,
			final Font pFont) {
		context = this;
		isFirstDown = false;
		isMoved = false;
		MainActivity.isInLevelSelector = true;
		float tempX = mInitialX + TILE_DIMENSION * 0.5f;
		float tempY = MainActivity.cameraHeight * 0.1f;

		int currentTileLevel = 1;

		for (int n = 0; n < mChapter; n++) {
			for (int i = 0; i < ROWS; i++) {

				for (int o = 0; o < COLUMNS; o++) {

					final boolean locked;

					if (currentTileLevel - 1 <= MainActivity.lastLevelPassed) {
						locked = false;
					} else {
						locked = true;
					}
					LevelTile levelTile = new LevelTile(tempX, tempY, locked,
							currentTileLevel, pTextureRegion, pFont);
					if (MainActivity.pass[currentTileLevel - 1]) {
						levelTile.setColor(0f, 1f, 0f);
					} else {
						levelTile.setColor(1f, 0f, 0f);
					}

					levelTile.attachText();

					mScene.registerTouchArea(levelTile);
					this.attachChild(levelTile);

					tempX = tempX + TILE_DIMENSION + TILE_PADDING;

					currentTileLevel++;
				}
				tempX = mInitialX + TILE_DIMENSION * 0.5f;
				tempY = tempY + TILE_DIMENSION + TILE_PADDING;
			}
			tempY = MainActivity.cameraHeight * 0.1f;
			currentTileLevel = 1;
			mInitialX = tempX + 1000;
			tempX = mInitialX + TILE_DIMENSION * 0.5f;
		}
		screenY = this.getY();
		screenX = this.getX();
		mScene.setOnSceneTouchListener(this);
		this.registerUpdateHandler((new IUpdateHandler() {
			@Override
			public void onUpdate(float pSecondsElapsed) {
				if (!isMoving) {
					screenX = context.getX();
				}
			}

			@Override
			public void reset() {
			}
		}));
	}

	public void show() {
		mHidden = false;

		if (!this.hasParent()) {
			mScene.attachChild(this);
		}
		this.setVisible(true);
	}

	public class LevelTile extends Sprite {

		private final boolean mIsLocked;
		private final int mLevelNumber;
		private final Font mFont;
		private Text mTileText;

		public LevelTile(float pX, float pY, boolean pIsLocked,
				int pLevelNumber, ITextureRegion pTextureRegion, Font pFont) {
			super(pX, pY, pTextureRegion, LevelSelector.this.mEngine
					.getVertexBufferObjectManager());

			this.mFont = pFont;
			this.mIsLocked = pIsLocked;
			this.mLevelNumber = pLevelNumber;
		}

		public void attachText() {

			String tileTextString = null;

			if (this.mTileText == null) {

				if (this.mIsLocked) {
					tileTextString = "-";
				} else {
					tileTextString = String.valueOf(this.mLevelNumber);
				}

				final float textPositionX = LevelSelector.this.TILE_DIMENSION * 0.5f;
				final float textPositionY = textPositionX;

				this.mTileText = new Text(textPositionX, textPositionY,
						this.mFont, tileTextString, tileTextString.length(),
						LevelSelector.this.mEngine
								.getVertexBufferObjectManager());

				this.attachChild(mTileText);
			}
		}

		@Override
		public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
				float pTouchAreaLocalX, float pTouchAreaLocalY) {

			if (!LevelSelector.this.mHidden) {

				if (pSceneTouchEvent.isActionUp()) {
					if (this.mIsLocked) {

					} else {

						if (this.mLevelNumber == 1) {
							Scene scene = new Scene();
							FirstLevel myLevel = new FirstLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 2) {
							Scene scene = new Scene();
							SecondLevel myLevel = new SecondLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 3) {
							Scene scene = new Scene();
							ThirdLevel myLevel = new ThirdLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 4) {
							Scene scene = new Scene();
							FourthLevel myLevel = new FourthLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 5) {
							Scene scene = new Scene();
							FifthLevel myLevel = new FifthLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 6) {
							Scene scene = new Scene();
							SixthLevel myLevel = new SixthLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 7) {
							Scene scene = new Scene();
							SeventhLevel myLevel = new SeventhLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 8) {
							Scene scene = new Scene();
							EightLevel myLevel = new EightLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 9) {
							Scene scene = new Scene();
							NineLevel myLevel = new NineLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						} else if (this.mLevelNumber == 10) {
							Scene scene = new Scene();
							TenthLevel myLevel = new TenthLevel(scene);
							myLevel.buildGame();
							scene.setBackground(new Background(0.09804f,
									0.6274f, 0.8784f));
							scene.registerUpdateHandler(MainActivity.mPhysicsWorld);
							mEngine.setScene(scene);
						}
					}
					return false;
				}
			}
			return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
					pTouchAreaLocalY);
		}
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {

		if (pSceneTouchEvent.getAction() == 0) {
			final Vector2 vec = Vector2Pool.obtain(pSceneTouchEvent.getX()
					/ PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT,
					pSceneTouchEvent.getY()
							/ PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
			this.clearEntityModifiers();
			firstX = vec.x;
			isFirstDown = true;
			Vector2Pool.recycle(vec);
		}
		if ((pSceneTouchEvent.getAction() == 1) && isFirstDown) {
			if (isMoved) {
				screenX = this.getX();
				screenY = this.getY();

				float pointsListX[] = { screenX,
						(Math.round((screenX / 1280)) * 1280) };

				final float pointsListY[] = { 0, 0 };
				final int controlPointCount = pointsListX.length;

				org.andengine.entity.modifier.PathModifier.Path path = new Path(
						controlPointCount);

				for (int i = 0; i < controlPointCount; i++) {
					final float positionX = pointsListX[i];
					final float positionY = pointsListY[i];
					path.to(positionX, positionY);
				}
				final float duration = 1;
				PathModifier pathModifier = new PathModifier(duration, path);

				this.registerEntityModifier(pathModifier);
				isMoving = false;
			}
			isMoved = false;
		}
		if (pSceneTouchEvent.isActionMove() && isFirstDown) {
			isMoved = true;
			final Vector2 vec = Vector2Pool.obtain(pSceneTouchEvent.getX()
					/ PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT,
					pSceneTouchEvent.getY()
							/ PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
			this.setPosition((float) (screenX + ((vec.x - firstX) * 20.0)),
					screenY);
			if (firstX != 0) {
				isMoving = true;
			}
			Vector2Pool.recycle(vec);
			return true;
		}

		return false;
	}
}