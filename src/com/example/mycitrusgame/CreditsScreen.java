package com.example.mycitrusgame;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.util.HorizontalAlign;

public class CreditsScreen {
	static Scene scene;

	public static void buildScreen(Scene mScene) {
		scene = mScene;
		float AUTOWRAP_WIDTH = 720 - 50 - 50;
		final Entity creditScreen = new Entity();
		Text creditText = new Text(
				0,
				0,
				MainActivity.mFont,
				"	Lorem ipsum dolor sit amet, " +
				"consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
				1000, new TextOptions(AutoWrap.LETTERS, AUTOWRAP_WIDTH, HorizontalAlign.CENTER, Text.LEADING_DEFAULT),MainActivity.engine.getVertexBufferObjectManager());
		creditScreen.attachChild(creditText);
		scene.attachChild(creditScreen);
	}
}
