package com.example.mycitrusgame;

import java.io.File;
import java.util.ArrayList;

import org.andengine.audio.music.Music;
import org.andengine.engine.Engine;
import org.andengine.engine.FixedStepEngine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.util.Vector2Pool;
import org.andengine.input.sensor.acceleration.AccelerationData;
import org.andengine.input.sensor.acceleration.IAccelerationListener;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.color.Color;

import android.content.Context;
import android.graphics.Typeface;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.example.mycitrusgame.levels.Levels;

public class MainActivity extends BaseGameActivity implements
		IAccelerationListener, IOnSceneTouchListener {
	public static int cameraWidth = 1280;
	public static int cameraHeight = 720;
	public static Scene mScene;
	public static boolean isInLevelSelector;
	public Boolean isTouched;
	public static Entity BNW;
	public static ArrayList<Levels> levelArray;
	public static Context context;
	public static FixedStepPhysicsWorld mPhysicsWorld;
	public static Engine engine;
	public Body ballBody;
	public Body targetBody;
	public Body grassGroundFirstLevelBody;
	public Body grassGroundSecondLevelBody;
	public Body grassGroundThirdLevelBody;
	public Body grassGroundFourthLevelBody;
	public Body grassGroundGroundLevelBody;
	public Body groundWallBody;
	public Body roofWallBody;
	public Body leftWallBody;
	public Body rightWallBody;
	public static DatabaseValue gameSettings;
	public static Sqllite dataBase;
	public static boolean isModFinished;
	public static Scene currentScene;
	public static boolean isCleaned;
	public static boolean isIngame;
	public static Music mMenuThemeMusic;
	public static int stars[];
	public static boolean pass[];
	public static Levels currentLevel;
	public static boolean sound;
	public static boolean vibrations;
	public static Font mFont;
	public static boolean isPause;
	public static int lastLevelPassed;

	public static ITextureRegion gateTR;
	public static ITextureRegion appleTR;
	public static ITextureRegion pauseMenuTR;
	public static ITextureRegion resetButtonTR;
	public static ITiledTextureRegion mButtonTextureRegion;
	public static ITextureRegion offOptionsTR;
	public static ITextureRegion onOptionsTR;
	public static ITextureRegion ballTR;
	public static ITextureRegion levelButtonTR;
	public static ITextureRegion targetTR;
	public static ITextureRegion enemyTR;
	public static ITextureRegion tenthLevelEnemyTR;
	public static ITextureRegion tenthLevelTR;
	public static ITextureRegion ninthLevelTR;
	public static ITextureRegion eightLevelTR;
	public static ITextureRegion eightLevelDiskTR;
	public static ITextureRegion seventhLevelTR;
	public static ITextureRegion enemyLevel7TR;
	public static ITextureRegion secondLevelUpperSpinnerTR;
	public static ITextureRegion secondLevelDownSpinnerTR;
	public static ITextureRegion grassGroundSixthLevel1TR;
	public static ITextureRegion grassGroundSixthLevel2TR;
	public static ITextureRegion grassGroundSixthLevel3TR;
	public static ITextureRegion grassGroundTRFifthLevel1TR;
	public static ITextureRegion grassGroundTRFifthLevel2TR;
	public static ITextureRegion yellowFifthLevelTR;
	public static ITextureRegion pinkFifthLevelTR;
	public static ITextureRegion grassGroundTRFourthLevel1TR;
	public static ITextureRegion grassGroundTRFourthLevel2TR;
	public static ITextureRegion diskTRFourthtLevel1TR;
	public static ITextureRegion firstLevelBgTR;
	public static ITextureRegion grassGroundTRFirstLevel1TR;
	public static ITextureRegion grassGroundTRSecondLevel1TR;
	public static ITextureRegion grassGroundTRThirdLevel1TR;
	public static ITextureRegion grassGroundTRForthLevel1TR;
	public static ITextureRegion grassGroundTRGoundLevel1TR;
	public static ITextureRegion grassGroundTRGoundLevel2TR;
	public static ITextureRegion grassGroundGroundLevel3TR;
	public static ITextureRegion grassGroundGroundLevel3FirstGroundTR;
	public static ITextureRegion grassGroundGroundLevel3SecondGroundTR;
	public static ITextureRegion grassGroundGroundLevel3ThirdGroundTR;
	public static ITextureRegion grassGroundGroundLevel3FourthGroundTR;
	public static ITextureRegion buttonTextureRegion;
	public static ITextureRegion menuBackground;

	BitmapTextureAtlas gateTA;
	BitmapTextureAtlas appleTA;
	BitmapTextureAtlas pauseMenuTA;
	BitmapTextureAtlas resetButtonTA;
	public static BuildableBitmapTextureAtlas offOptionsTA;
	public static BitmapTextureAtlas onOptionsTA;
	BitmapTextureAtlas tenthLevelEnemyTA;
	BitmapTextureAtlas tenthLevelTA;
	BitmapTextureAtlas ninthLevelTA;
	BitmapTextureAtlas eightLevelTA;
	BitmapTextureAtlas eightLevelDiskTA;
	BitmapTextureAtlas seventhLevelTA;
	BitmapTextureAtlas enemyLevel7TA;
	BitmapTextureAtlas levelButtonAT;
	BitmapTextureAtlas firstLevelBgAtlas;
	BitmapTextureAtlas ballTA;
	BitmapTextureAtlas targetTA;
	BitmapTextureAtlas enemyTA;
	BitmapTextureAtlas grassGroundSixthLevel1TA;
	BitmapTextureAtlas grassGroundSixthLevel2TA;
	BitmapTextureAtlas grassGroundSixthLevel3TA;
	BitmapTextureAtlas grassGroundTRFifthLevel1TA;
	BitmapTextureAtlas grassGroundTRFifthLevel2TA;
	BitmapTextureAtlas yellowFifthLevelTA;
	BitmapTextureAtlas pinkFifthLevelTA;
	BitmapTextureAtlas grassGroundTRFourthLevel1TA;
	BitmapTextureAtlas grassGroundTRFourthLevel2TA;
	BitmapTextureAtlas diskTRFourthtLevel1TA;
	BitmapTextureAtlas secondLevelUpperSpinnerAT;
	BitmapTextureAtlas secondLevelDownSpinnerAT;
	BitmapTextureAtlas grassGroundFirstLevel1TA;
	BitmapTextureAtlas grassGroundSecondLevel1TA;
	BitmapTextureAtlas grassGroundThirdLevel1TA;
	BitmapTextureAtlas grassGroundFourthLevel1TA;
	BitmapTextureAtlas grassGroundGroundLevel1TA;
	BitmapTextureAtlas grassGroundGroundLevel2TA;
	BitmapTextureAtlas grassGroundGroundLevel3TA;
	BitmapTextureAtlas grassGroundGroundLevel3FirstGroundTA;
	BitmapTextureAtlas grassGroundGroundLevel3SecondGroundTA;
	BitmapTextureAtlas grassGroundGroundLevel3ThirdGroundTA;
	BitmapTextureAtlas grassGroundGroundLevel3FourthGroundTA;
	BitmapTextureAtlas backgroundAtlas;
	BitmapTextureAtlas beginButtonAtlas;

	public static LevelSelector levelSelector;
	public static Scene backScene;

	long lastShakeTime = 0;
	float lastShakeX, lastShakeY, lastShakeZ = 0;

	private final static ParallelEntityModifier mMoveInModifier = new ParallelEntityModifier(
			new MoveXModifier(3, 720, 0), new RotationModifier(3, 0, 360),
			new ScaleModifier(3, 0, 1));
	private final static ParallelEntityModifier mMoveOutModifier = new ParallelEntityModifier(
			new MoveXModifier(3, 0, -720), new RotationModifier(3, 360, 0),
			new ScaleModifier(3, 1, 0));

	public static void updateDatabase() {
		MainActivity.dataBase.createEntryInGameTable(
				String.valueOf(lastLevelPassed), sound, vibrations);
	}

	@Override
	public boolean onKeyDown(final int keyCode, final KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			if (isIngame) {
				if (MainActivity.isPause) {
					for (int i = 0; i < MainActivity.currentScene
							.getChildByIndex(0).getChildCount(); i++) {
						MainActivity.currentScene.getChildByIndex(0)
								.getChildByIndex(i).setColor(0.4f, 0.4f, 0.4f);
					}
					MainActivity.mMenuThemeMusic.setLooping(false);
					MainActivity.currentScene.setIgnoreUpdate(true);
					PauseScreen.buildScreen(MainActivity.currentScene);
					MainActivity.isPause = false;
				} else {
					PauseScreen.mScene
							.unregisterTouchArea(PauseScreen.optionsGameSprite);
					PauseScreen.mScene
							.unregisterTouchArea(PauseScreen.quitGameSprite);
					PauseScreen.mScene
							.unregisterTouchArea(PauseScreen.resumeGameSprite);
					engine.runOnUpdateThread(new Runnable() {
						@Override
						public void run() {
							PauseScreen.mScene
									.detachChild(PauseScreen.optionsGameSprite);
							PauseScreen.mScene
									.detachChild(PauseScreen.quitGameSprite);
							PauseScreen.mScene
									.detachChild(PauseScreen.resumeGameSprite);
							PauseScreen.mScene.detachChild(PauseScreen.layer);
						}
					});

					for (int i = 0; i < MainActivity.currentScene
							.getChildByIndex(0).getChildCount(); i++) {
						MainActivity.currentScene.getChildByIndex(0)
								.getChildByIndex(i).setColor(1f, 1f, 1f);
					}
					MainActivity.mMenuThemeMusic.setLooping(true);
					MainActivity.currentScene.setIgnoreUpdate(false);
					MainActivity.isPause = true;
				}

			} else if (isInLevelSelector) {

				MenuScreen.buildMenuScreen(mScene);
				mEngine.setScene(mScene);
				isInLevelSelector = false;
			} else {
				System.exit(0);
			}
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	@Override
	public Engine onCreateEngine(final EngineOptions pEngineOptions) {
		context = this;
		sound = false;
		isInLevelSelector = false;
		vibrations = false;
		levelArray = new ArrayList<Levels>();
		engine = new FixedStepEngine(pEngineOptions, 60);
		return engine;
	}

	@Override
	public EngineOptions onCreateEngineOptions() {
		MainActivity.isPause = true;
		isTouched = false;
		pass = new boolean[20];
		stars = new int[20];
		isIngame = false;
		isModFinished = true;
		EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(),
				new SmoothCamera(0, 0, cameraWidth, cameraHeight, 50, 50, 5));
		engineOptions.getRenderOptions().setDithering(true);
		engineOptions.getAudioOptions().setNeedsSound(true);
		engineOptions.getAudioOptions().setNeedsMusic(true);
		engineOptions.getRenderOptions().setMultiSampling(true);
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		return engineOptions;
	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception {
		File file = context.getApplicationContext().getDatabasePath("RNDB.db");
		MainActivity.dataBase = new Sqllite(this);
		if (file.exists()) {
			MainActivity.dataBase = MainActivity.dataBase.openExisting();
		} else {
			MainActivity.dataBase = MainActivity.dataBase.open();
		}
		gameSettings = dataBase.getDataForGame();
		if (gameSettings == null) {
			gameSettings = new DatabaseValue("0", "true", "true");
		}
		lastLevelPassed = Integer.parseInt(gameSettings.lastLevelPassed);
		sound = Boolean.parseBoolean(gameSettings.optionsSound);
		vibrations = Boolean.parseBoolean(gameSettings.optionsVibration);
		loadSpalshResources();
		MainActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), "Button Pressed!",
						Toast.LENGTH_SHORT).show();
			}
		});

		pOnCreateResourcesCallback.onCreateResourcesFinished();

	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception {

		mScene = new Scene();

		mScene.setTouchAreaBindingOnActionDownEnabled(true);
		backScene = mScene;
		pOnCreateSceneCallback.onCreateSceneFinished(mScene);
	}

	public void loadSpalshResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		gateTA = new BitmapTextureAtlas(this.getTextureManager(), 100, 200);
		appleTA = new BitmapTextureAtlas(this.getTextureManager(), 26, 26);
		pauseMenuTA = new BitmapTextureAtlas(this.getTextureManager(), 350, 450);
		offOptionsTA = new BuildableBitmapTextureAtlas(
				this.getTextureManager(), 128, 64);
		onOptionsTA = new BitmapTextureAtlas(this.getTextureManager(), 64, 64);
		resetButtonTA = new BitmapTextureAtlas(this.getTextureManager(), 128,
				128);
		levelButtonAT = new BitmapTextureAtlas(this.getTextureManager(), 64, 64);
		ballTA = new BitmapTextureAtlas(this.getTextureManager(), 64, 64);
		targetTA = new BitmapTextureAtlas(this.getTextureManager(), 128, 128);
		enemyTA = new BitmapTextureAtlas(this.getTextureManager(), 128, 128);
		firstLevelBgAtlas = new BitmapTextureAtlas(this.getTextureManager(),
				1280, 720);
		backgroundAtlas = new BitmapTextureAtlas(this.getTextureManager(),
				1280, 700);
		beginButtonAtlas = new BitmapTextureAtlas(this.getTextureManager(),
				400, 150);

		// -----------------------------------------------------------------------
		// --------FIRST LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------
		grassGroundFirstLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 1152, 64);
		grassGroundSecondLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 1152, 64);
		grassGroundThirdLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 1152, 64);
		grassGroundFourthLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 1152, 64);
		grassGroundGroundLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 1280, 64);
		grassGroundTRFirstLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundFirstLevel1TA, this,
						"grassGround1.png", 0, 0);
		grassGroundTRSecondLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundSecondLevel1TA, this,
						"grassGround2.png", 0, 0);
		grassGroundTRThirdLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundThirdLevel1TA, this,
						"grassGround3.png", 0, 0);
		grassGroundTRForthLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundFourthLevel1TA, this,
						"grassGround4.png", 0, 0);
		grassGroundTRGoundLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundGroundLevel1TA, this,
						"grassGround5.png", 0, 0);
		// -----------------------------------------------------------------------
		// --------SECOND
		// LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------
		grassGroundGroundLevel2TA = new BitmapTextureAtlas(
				this.getTextureManager(), 1280, 720);
		secondLevelUpperSpinnerAT = new BitmapTextureAtlas(
				this.getTextureManager(), 300, 110);
		secondLevelDownSpinnerAT = new BitmapTextureAtlas(
				this.getTextureManager(), 300, 110);

		grassGroundTRGoundLevel2TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundGroundLevel2TA, this,
						"secondLevel.png", 0, 0);
		secondLevelUpperSpinnerTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(secondLevelUpperSpinnerAT, this,
						"upperDisk.png", 0, 0);
		secondLevelDownSpinnerTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(secondLevelDownSpinnerAT, this,
						"downDisk.png", 0, 0);
		// -----------------------------------------------------------------------
		// --------THIRD LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------
		grassGroundGroundLevel3TA = new BitmapTextureAtlas(
				this.getTextureManager(), 1280, 720);
		grassGroundGroundLevel3FirstGroundTA = new BitmapTextureAtlas(
				this.getTextureManager(), 370, 174);
		grassGroundGroundLevel3SecondGroundTA = new BitmapTextureAtlas(
				this.getTextureManager(), 370, 174);
		grassGroundGroundLevel3ThirdGroundTA = new BitmapTextureAtlas(
				this.getTextureManager(), 370, 174);
		grassGroundGroundLevel3FourthGroundTA = new BitmapTextureAtlas(
				this.getTextureManager(), 370, 174);

		grassGroundGroundLevel3TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundGroundLevel3TA, this,
						"thirdLevel.png", 0, 0);
		grassGroundGroundLevel3FirstGroundTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundGroundLevel3FirstGroundTA, this,
						"thirdLevelFirstGround.png", 0, 0);
		grassGroundGroundLevel3SecondGroundTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundGroundLevel3SecondGroundTA, this,
						"thirdLevelSecondGround.png", 0, 0);
		grassGroundGroundLevel3ThirdGroundTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundGroundLevel3ThirdGroundTA, this,
						"thirdLevelThirdGround.png", 0, 0);
		grassGroundGroundLevel3FourthGroundTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundGroundLevel3FourthGroundTA, this,
						"thirdLevelFourthGround.png", 0, 0);
		// -----------------------------------------------------------------------
		// --------FOURTH
		// LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------

		grassGroundTRFourthLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 420, 350);
		grassGroundTRFourthLevel2TA = new BitmapTextureAtlas(
				this.getTextureManager(), 500, 400);
		diskTRFourthtLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 400, 300);

		grassGroundTRFourthLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundTRFourthLevel1TA, this,
						"fourthLevelGround1.png", 0, 0);
		grassGroundTRFourthLevel2TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundTRFourthLevel2TA, this,
						"fourthLevelGround2.png", 0, 0);
		diskTRFourthtLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(diskTRFourthtLevel1TA, this,
						"fourthLevelDisk.png", 0, 0);
		// -----------------------------------------------------------------------
		// -----------------------------------------------------------------------
		// --------FIFTH LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------

		grassGroundTRFifthLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 700, 720);
		grassGroundTRFifthLevel2TA = new BitmapTextureAtlas(
				this.getTextureManager(), 300, 200);
		yellowFifthLevelTA = new BitmapTextureAtlas(this.getTextureManager(),
				120, 120);
		pinkFifthLevelTA = new BitmapTextureAtlas(this.getTextureManager(),
				120, 120);

		grassGroundTRFifthLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundTRFifthLevel1TA, this,
						"fifthGround.png", 0, 0);
		grassGroundTRFifthLevel2TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundTRFifthLevel2TA, this,
						"fifthground2.png", 0, 0);
		yellowFifthLevelTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(yellowFifthLevelTA, this,
						"fifthYewollArea.png", 0, 0);
		pinkFifthLevelTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(pinkFifthLevelTA, this, "fifthPinkArea.png",
						0, 0);
		// -----------------------------------------------------------------------

		// -----------------------------------------------------------------------
		// -----------------------------------------------------------------------
		// --------SIXTH LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------

		grassGroundSixthLevel1TA = new BitmapTextureAtlas(
				this.getTextureManager(), 300, 720);
		grassGroundSixthLevel2TA = new BitmapTextureAtlas(
				this.getTextureManager(), 300, 300);
		grassGroundSixthLevel3TA = new BitmapTextureAtlas(
				this.getTextureManager(), 300, 300);

		grassGroundSixthLevel1TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundSixthLevel1TA, this,
						"sixthGroundSpinner.png", 0, 0);
		grassGroundSixthLevel2TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundSixthLevel2TA, this,
						"sixthGround1.png", 0, 0);
		grassGroundSixthLevel3TR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(grassGroundSixthLevel3TA, this,
						"sixthGround2.png", 0, 0);
		// -----------------------------------------------------------------------

		// -----------------------------------------------------------------------
		// --------SEVENTH
		// LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------

		seventhLevelTA = new BitmapTextureAtlas(this.getTextureManager(), 1280,
				720);
		enemyLevel7TA = new BitmapTextureAtlas(this.getTextureManager(), 50,
				200);

		seventhLevelTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(seventhLevelTA, this, "levelSeven.png", 0, 0);
		enemyLevel7TR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				enemyLevel7TA, this, "enemy7.png", 0, 0);

		// -----------------------------------------------------------------------

		// -----------------------------------------------------------------------
		// --------EIGHT LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------

		eightLevelTA = new BitmapTextureAtlas(this.getTextureManager(), 1280,
				720);
		eightLevelDiskTA = new BitmapTextureAtlas(this.getTextureManager(),
				350, 350);

		eightLevelTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				eightLevelTA, this, "levelEight.png", 0, 0);
		eightLevelDiskTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(eightLevelDiskTA, this, "levelEightDisk.png",
						0, 0);

		// -----------------------------------------------------------------------

		// -----------------------------------------------------------------------
		// --------NINTH LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------

		ninthLevelTA = new BitmapTextureAtlas(this.getTextureManager(), 1280,
				720);

		ninthLevelTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				ninthLevelTA, this, "nineLevel.png", 0, 0);

		// -----------------------------------------------------------------------
		// -----------------------------------------------------------------------
		// --------TENTH LEVEL--------------------------------------------------
		// -----------------------------------------------------------------------

		tenthLevelTA = new BitmapTextureAtlas(this.getTextureManager(), 1280,
				720);
		tenthLevelEnemyTA = new BitmapTextureAtlas(this.getTextureManager(),
				35, 35);

		tenthLevelTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				ninthLevelTA, this, "tenthLevel.png", 0, 0);
		tenthLevelEnemyTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(ninthLevelTA, this, "enemy10level.png", 0, 0);

		// -----------------------------------------------------------------------
		gateTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gateTA,
				this, "gate.png", 0, 0);
		appleTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				appleTA, this, "apple.png", 0, 0);
		resetButtonTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				resetButtonTA, this, "resetButton.png", 0, 0);
		levelButtonTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				levelButtonAT, this, "levelButton.png", 0, 0);
		ballTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(ballTA,
				this, "ball.png", 0, 0);
		targetTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				targetTA, this, "target.png", 0, 0);
		enemyTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				enemyTA, this, "enemy.png", 0, 0);
		onOptionsTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				onOptionsTA, this, "onButton.png", 0, 0);
		firstLevelBgTR = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(firstLevelBgAtlas, this, "bg1lv.png", 0, 0);
		pauseMenuTR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				pauseMenuTA, this, "pauseMenu.png", 0, 0);
		menuBackground = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(backgroundAtlas, this, "background.png", 0, 0);
		buttonTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(beginButtonAtlas, this, "begin.png", 0, 0);

		mFont = FontFactory.create(mEngine.getFontManager(),
				mEngine.getTextureManager(), 256, 256,
				Typeface.create(Typeface.DEFAULT, Typeface.NORMAL), 32f, true,
				Color.WHITE_ABGR_PACKED_INT);
		mButtonTextureRegion = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(offOptionsTA, getAssets(),
						"soundOptions.png", 2, 1);

		try {
			offOptionsTA
					.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
							0, 0, 0));
		} catch (TextureAtlasBuilderException e) {
			e.printStackTrace();
		}

		gateTA.load();
		appleTA.load();
		pauseMenuTA.load();
		resetButtonTA.load();
		onOptionsTA.load();
		offOptionsTA.load();
		tenthLevelTA.load();
		tenthLevelEnemyTA.load();
		ninthLevelTA.load();
		eightLevelTA.load();
		eightLevelDiskTA.load();
		enemyLevel7TA.load();
		seventhLevelTA.load();
		mFont.load();
		grassGroundSixthLevel1TA.load();
		grassGroundSixthLevel2TA.load();
		grassGroundSixthLevel3TA.load();
		grassGroundTRFifthLevel1TA.load();
		grassGroundTRFifthLevel2TA.load();
		yellowFifthLevelTA.load();
		pinkFifthLevelTA.load();
		grassGroundTRFourthLevel1TA.load();
		grassGroundTRFourthLevel2TA.load();
		diskTRFourthtLevel1TA.load();
		secondLevelUpperSpinnerAT.load();
		secondLevelDownSpinnerAT.load();
		levelButtonAT.load();
		targetTA.load();
		enemyTA.load();
		ballTA.load();
		grassGroundGroundLevel3FirstGroundTA.load();
		grassGroundGroundLevel3TA.load();
		grassGroundGroundLevel3SecondGroundTA.load();
		grassGroundGroundLevel3ThirdGroundTA.load();
		grassGroundGroundLevel3FourthGroundTA.load();
		firstLevelBgAtlas.load();
		grassGroundFirstLevel1TA.load();
		grassGroundSecondLevel1TA.load();
		grassGroundThirdLevel1TA.load();
		grassGroundFourthLevel1TA.load();
		grassGroundGroundLevel1TA.load();
		grassGroundGroundLevel2TA.load();
		beginButtonAtlas.load();
		backgroundAtlas.load();
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {

		MainActivity.this.runOnUiThread(new Runnable() {

			public void run() {

			}
		});
		mPhysicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0f,
				-SensorManager.GRAVITY_EARTH * 2), false, 10, 5);
		MenuScreen.buildMenuScreen(mScene);
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onAccelerationAccuracyChanged(AccelerationData pAccelerationData) {

	}

	@Override
	public void onResumeGame() {

		if (mEngine != null) {

			super.onResumeGame();
			this.enableAccelerationSensor(this);
		}
	}

	@Override
	public void onPauseGame() {

		super.onPauseGame();
		this.disableAccelerationSensor();
	}

	@Override
	public void onAccelerationChanged(AccelerationData pAccelerationData) {
		final Vector2 gravity = Vector2Pool.obtain(
				pAccelerationData.getX() * 10, pAccelerationData.getY() * 10);
		MainActivity.mPhysicsWorld.setGravity(gravity);
		Vector2Pool.recycle(gravity);

		long currentTime = System.currentTimeMillis();

		if ((currentTime - lastShakeTime) > 100) {

			long diffTime = currentTime - lastShakeTime;

			lastShakeTime = currentTime;

			float x = pAccelerationData.getX();
			float y = pAccelerationData.getY();
			float z = pAccelerationData.getZ();

			float speed = Math.abs(x + y + z - lastShakeX - lastShakeY
					- lastShakeZ)
					/ diffTime * 10000;

			if (speed > 500) {
				if (isIngame) {
					Log.i("isModFIn", "" + isModFinished);
					Log.i("isBallSmall", "" + currentLevel.isBallSmall);
					if (currentLevel.isBallSmall && isModFinished) {
						currentLevel.isBallSmall = false;
						currentLevel.isBallChanged = false;
					} else if (isModFinished) {
						currentLevel.isBallSmall = true;
						currentLevel.isBallChanged = false;
					}
				}
			}

			lastShakeX = x;
			lastShakeY = y;
			lastShakeZ = z;
		}
	}

	public static void setLayer(IEntity pLayerIn, IEntity pLayerOut) {

		if (!pLayerIn.isVisible()) {
			pLayerIn.setVisible(true);
		}

		mMoveInModifier.reset();
		mMoveOutModifier.reset();

		pLayerIn.registerEntityModifier(mMoveInModifier);
		pLayerOut.registerEntityModifier(mMoveOutModifier);
	}

}
