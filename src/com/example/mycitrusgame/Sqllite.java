package com.example.mycitrusgame;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Sqllite {
	public static final String KEY_LAST_LEVEL_PASSED = "imageURL";
	public static final String KEY_OPTIONS_SOUND = "sectionId";
	public static final String KEY_OPTIONS_VIBRATIONS = "detailedText";
	public static final String KEY_ROWID = "id";
	public static final String DATABASE_NAME = "RNDB.db";
	public static final String GAME_TABLE = "game";
	@SuppressLint("SdCardPath")
	private static final String DB_PATH = "/data/data/com.example.mycitrusgame/databases/RNDB.db";
	public static final int DATABASE_VERSION = 1;

	private DbHelper ourHelper;
	private final Context ourContext;
	private SQLiteDatabase ourDatabase;

	private static class DbHelper extends SQLiteOpenHelper {

		public DbHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL("CREATE TABLE " + GAME_TABLE + " (" + KEY_ROWID
					+ " INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, "
					+ KEY_LAST_LEVEL_PASSED + " VARCHAR NOT NULL, "
					+ KEY_OPTIONS_SOUND + " VARCHAR NOT NULL, "
					+ KEY_OPTIONS_VIBRATIONS + " VARCHAR NOT NULL " + ");");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + GAME_TABLE);
			onCreate(db);
		}

	}

	public Sqllite(Context context) {
		ourContext = context;
	}

	public Sqllite open() {
		ourHelper = new DbHelper(ourContext);
		ourDatabase = ourHelper.getWritableDatabase();
		return this;
	}

	public Sqllite openExisting() {
		ourDatabase = SQLiteDatabase.openDatabase(DB_PATH, null,
				SQLiteDatabase.OPEN_READWRITE);
		return this;
	}

	public void close() {
		ourHelper.close();
	}

	public long createEntryInGameTable(String lastLevelPassed,
			boolean options_sound, boolean options_vibrations) {
		ContentValues cv = new ContentValues();
		cv.put(KEY_LAST_LEVEL_PASSED, lastLevelPassed);
		cv.put(KEY_OPTIONS_SOUND, options_sound);
		cv.put(KEY_OPTIONS_VIBRATIONS, options_vibrations);
		return ourDatabase.insert(GAME_TABLE, null, cv);
	}

	public String[] getExistingIdsForGameTable() {
		String rows[] = new String[] { KEY_ROWID };
		Cursor c = ourDatabase.query(GAME_TABLE, rows, null, null, null, null,
				null);
		String result[] = new String[(int) getSizeOfTableForGameTable()];
		int title = c.getColumnIndex(KEY_ROWID);
		int i = 0;
		if (c.moveToFirst()) {
			for (i = 0; !c.isAfterLast(); c.moveToNext()) {
				result[i] = c.getString(title);
				i += 1;
			}
		}
		return result;
	}

	public DatabaseValue getDataForGame() {

		DatabaseValue result = null;
		String rows[] = new String[] { KEY_LAST_LEVEL_PASSED,
				KEY_OPTIONS_SOUND, KEY_OPTIONS_VIBRATIONS };
		Cursor c = ourDatabase.query(GAME_TABLE, rows, null, null, null, null,
				null);

		int lastLevelPassed = c.getColumnIndex(KEY_LAST_LEVEL_PASSED);
		int optionsSound = c.getColumnIndex(KEY_OPTIONS_SOUND);
		int optionsVibration = c.getColumnIndex(KEY_OPTIONS_VIBRATIONS);
		if (c.getCount() != 0) {
			for (c.moveToFirst(); c.isAfterLast(); c.moveToNext()) {
				result = new DatabaseValue(c.getString(lastLevelPassed),
						c.getString(optionsSound),
						c.getString(optionsVibration));
			}
		}
		return result;
	}

	public long getSizeOfTableForGameTable() {
		long index = 0;
		String rows[] = new String[] { KEY_ROWID };
		Cursor cursor = ourDatabase.query(GAME_TABLE, rows, "1=1", null, null,
				null, null);
		index = cursor.getCount();
		cursor.close();
		return index;
	}

}