package com.example.mycitrusgame;

import org.andengine.entity.Entity;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;

public class PauseScreen {

	static Scene mScene;
	static Sprite pauseMenu;
	static ButtonSprite resumeGameSprite;
	static ButtonSprite quitGameSprite;
	static ButtonSprite optionsGameSprite;
	static Entity layer;

	public static void buildScreen(Scene scene) {

		layer = new Entity();
		mScene = scene;
		pauseMenu =  new Sprite((MainActivity.cameraWidth/2) - 350/2,
				(MainActivity.cameraHeight/2) - 450/2, MainActivity.pauseMenuTR,
				MainActivity.engine.getVertexBufferObjectManager());
		resumeGameSprite = new ButtonSprite(MainActivity.cameraWidth - 900,
				100, MainActivity.buttonTextureRegion,
				MainActivity.engine.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {
					mScene.unregisterTouchArea(optionsGameSprite);
					mScene.unregisterTouchArea(quitGameSprite);
					mScene.unregisterTouchArea(resumeGameSprite);
					MainActivity.engine.runOnUpdateThread(new Runnable() {
						@Override
						public void run() {
							mScene.detachChild(optionsGameSprite);
							mScene.detachChild(quitGameSprite);
							mScene.detachChild(resumeGameSprite);
							mScene.detachChild(layer);
						}
					});
					MainActivity.mMenuThemeMusic.setLooping(true);
					for (int i = 0; i < MainActivity.currentScene.getChildByIndex(0).getChildCount(); i++) {
						MainActivity.currentScene.getChildByIndex(0).getChildByIndex(i).setColor(1f,
								1f, 1f);
					}
					MainActivity.currentScene.setIgnoreUpdate(false);
					MainActivity.isPause = true;
				}
				;

				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		quitGameSprite = new ButtonSprite(MainActivity.cameraWidth - 900, 300,
				MainActivity.buttonTextureRegion,
				MainActivity.engine.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {

				}
				;

				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		optionsGameSprite = new ButtonSprite(MainActivity.cameraWidth - 900,
				500, MainActivity.buttonTextureRegion,
				MainActivity.engine.getVertexBufferObjectManager()) {
			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY) {

				if (pSceneTouchEvent.isActionDown()) {

				}
				;

				return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX,
						pTouchAreaLocalY);
			}
		};
		layer.attachChild(pauseMenu);
		mScene.attachChild(layer);
		mScene.registerTouchArea(optionsGameSprite);
		mScene.registerTouchArea(quitGameSprite);
		mScene.registerTouchArea(resumeGameSprite);
		mScene.attachChild(optionsGameSprite);
		mScene.attachChild(quitGameSprite);
		mScene.attachChild(resumeGameSprite);
	
	}

}
